﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectShooter : MonoBehaviour
{
    public Rigidbody2D toShoot;
    public float velocity;
    public float delay = 0.1f;
    public int count;
    private float lastShot;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Fire1")>0) {
            if (Time.time - lastShot > delay) {
                
                Projectile p = ProjectilePool.GetProjectile();
                if (p != null) {
                    p.gameObject.SetActive(true);
                    p.TriggerChild.SetActive(true);
                    p.TriggerChild.layer = 14;
                    p.transform.position = transform.position;
                    p.lifeSpan = 5;
                    p.Rigidbody.velocity = ((Vector2)(Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position).normalized) * velocity;
                    count++;
                    lastShot = Time.time;
                }
            }
           
        }
    }
}
