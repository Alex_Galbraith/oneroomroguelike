﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Wrapper for displaying enumerated float arrays
/// </summary>
[Serializable]
public struct StatWrapperFloat {
    [SerializeField]
    public readonly string suffix;
    [SerializeField]
    public readonly Type enumType;
    [SerializeField]
    float[] data;
    public string[] names;
    public StatWrapperFloat(Type enumType, string suffix = "") {
        this.enumType = enumType;
        if (enumType.BaseType != typeof(Enum)) {
            throw new ArgumentException("First type must be Enum");
        }

        this.suffix = suffix;
        data = new float[Enum.GetValues(enumType).Length];
        names = Enum.GetNames(enumType);
    }
    public float this[int i] {
        get { return data[i]; }
        set { data[i] = value; }
    }
    public float this[IConvertible i] {
        get { return data[(int)i]; }
        set { data[(int)i] = value; }
    }
    public int Length {
        get { return data == null ? 0 : data.Length; }
    }
}

/// <summary>
/// Wrapper for displaying enumerated int arrays
/// </summary>
[Serializable]
public class StatWrapperInt {
    [SerializeField]
    public readonly string suffix;
    [SerializeField]
    public readonly Type enumType;
    [SerializeField]
    int[] data;
    public string[] names;
    public StatWrapperInt(Type enumType, string suffix = "") {
        this.enumType = enumType;
        if (enumType.BaseType != typeof(Enum)) {
            throw new ArgumentException("First type must be Enum");
        }
        this.suffix = suffix;
        data = new int[Enum.GetValues(enumType).Length];
        names = Enum.GetNames(enumType);
    }
    public int this[int i] {
        get { return data[i]; }
        set { data[i] = value; }
    }
    public int this[IConvertible i] {
        get { return data[(int)i]; }
        set { data[(int)i] = value; }
    }
    public int Length {
        get { return data == null ? 0 : data.Length; }
    }
}