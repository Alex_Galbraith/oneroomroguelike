﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class ExtrudeTilesWizard : ScriptableWizard
{
    public Vector2Int offset;
    public int padding = 1;
    public int extrude = 1;
    public Vector2Int tileSize = Vector2Int.one * 16;
    public Texture2D toExtrude;
    public string path;
    public bool mip = false;
    [MenuItem("Tools/Sprites/Extrude TileMap")]
    public static void StartPadding() {
        ScriptableWizard.DisplayWizard<ExtrudeTilesWizard>("Extrude TileMap");
    }

    public void OnValidate() {
        path = AssetDatabase.GetAssetPath(toExtrude);
        if (!path.Contains("."))
            return;
        string[] parts = path.Split('.');
        path = "";
        for (int i = 0; i < parts.Length - 1; i++)
            path += parts[i];
        path += "_extruded." + parts[parts.Length - 1];
    }

    private RectInt CreateRectFrom(int cellX, int cellY) {
        int cellSX = padding + tileSize.x;
        int cellSY = padding + tileSize.y;
        Vector2Int cellPos = new Vector2Int(cellX * cellSX, cellY * cellSY);
        Vector2Int cellSize = new Vector2Int(tileSize.x, tileSize.y);
        return new RectInt(cellPos+offset, cellSize);
    }

    private RectInt CreateRectTo(int cellX, int cellY) {
        int cellSX = extrude * 2 + tileSize.x;
        int cellSY = extrude * 2 + tileSize.y;
        Vector2Int cellPos = new Vector2Int(cellX * cellSX, cellY * cellSY);
        cellPos += Vector2Int.one * extrude;
        Vector2Int cellSize = new Vector2Int(tileSize.x, tileSize.y);
        return new RectInt(cellPos, cellSize);
    }

    private void ExtrudeRect(RectInt fromR, RectInt toR, Texture2D from, Texture2D to, int extrude) {
        //Loop x
        //Bot
        for(int x = fromR.min.x, x2 = toR.min.x; x < fromR.max.x; x++, x2++) {
            ExtrudePixel(from, to, new Vector2Int(x, fromR.min.y), new Vector2Int(x2, toR.min.y), Vector2Int.down * extrude);
        }
        //Top
        for (int x = fromR.min.x, x2 = toR.min.x; x < fromR.max.x; x++, x2++) {
            ExtrudePixel(from, to, new Vector2Int(x, fromR.max.y-1), new Vector2Int(x2, toR.max.y - 1), Vector2Int.up * extrude);
        }
        //Loop y
        //Left
        for (int y = fromR.min.y, y2=toR.min.y; y < fromR.max.y; y++, y2++) {
            ExtrudePixel(from, to, new Vector2Int(fromR.min.x, y), new Vector2Int(toR.min.x, y2), Vector2Int.left * extrude);
        }
        //Right
        for (int y = fromR.min.y, y2 = toR.min.y; y < fromR.max.y; y++, y2++) {
            ExtrudePixel(from, to, new Vector2Int(fromR.max.x-1, y), new Vector2Int(toR.max.x - 1, y2), Vector2Int.right * extrude);
        }
    }

    private void CopyRect(RectInt fromR, RectInt toR,Texture2D from, Texture2D to) {
        for (int x = 0; x < fromR.max.x - fromR.min.x; x++) {
            for (int y = 0; y < fromR.max.y - fromR.min.y; y++) {
                Color c = from.GetPixel(fromR.min.x + x, fromR.min.y + y);
                to.SetPixel(toR.min.x + x, toR.min.y + y, c);
            }
        }
    }

    private void ExtrudePixel(Texture2D from, Texture2D to, Vector2Int posFrom, Vector2Int posTo, Vector2Int dir) {
        Color c  = from.GetPixel(posFrom.x, posFrom.y);
        to.SetPixel(posTo.x + dir.x, posTo.y + dir.y, c);
    }

    public void OnWizardCreate() {
        int width = toExtrude.width;
        int height = toExtrude.height;
        int cellsX = (width - Mathf.Max(0,offset.x-padding)) / (padding  + tileSize.x);
        int cellsY = (height - Mathf.Max(0, offset.y - padding)) / (padding  + tileSize.y);

        if (!toExtrude.isReadable) {
            Debug.LogError("Texture unreadable");
            return;
        }

        if (!(toExtrude.format == TextureFormat.ARGB32 || toExtrude.format == TextureFormat.RGBA32 || toExtrude.format == TextureFormat.RGB24
            || toExtrude.format == TextureFormat.Alpha8 || toExtrude.format == TextureFormat.RFloat || toExtrude.format == TextureFormat.RGBAFloat
            || toExtrude.format == TextureFormat.RGFloat || toExtrude.format == TextureFormat.RGB9e5Float)) {
            Debug.LogError("Wrong format, must be RBBA32, ARGB32, RGB24, Alpha8, or a float format.");
            return;
            
        }
        Texture2D newTex = new Texture2D(cellsX * (tileSize.x+2*extrude), cellsY * (tileSize.y + 2 * extrude), toExtrude.format, mip);

        for (int ext = 0; ext<extrude; ext++) {
            for(int x = 0; x<cellsX; x++) {
                for (int y = 0; y < cellsX; y++) {
                    RectInt r1 = CreateRectFrom(x, y);
                    RectInt r2 = CreateRectTo(x, y);
                    CopyRect(r1, r2, toExtrude, newTex);
                    ExtrudeRect(r1, r2, toExtrude, newTex, ext + 1);
                }
            }
        }


        byte[] bytes = newTex.EncodeToPNG();
        DestroyImmediate(newTex);
        File.WriteAllBytes(path, bytes);
        Debug.Log("Wrote to: " + path);
    }
}
