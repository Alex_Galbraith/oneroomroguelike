﻿using UnityEngine;
using UnityEditor;
using System;

[CustomPropertyDrawer(typeof(StatWrapperFloat))]
[CustomPropertyDrawer(typeof(StatWrapperInt))]
class StatWrapperFloatEditor : PropertyDrawer {

    public static readonly float ITEM_HEIGHT = 20;

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
        if (!property.isExpanded)
            return base.GetPropertyHeight(property, label);
        StatWrapperFloat wrapper = (StatWrapperFloat)fieldInfo.GetValue(property.serializedObject.targetObject);
        float baseHeight = base.GetPropertyHeight(property, label);
        return ITEM_HEIGHT * (wrapper.Length + 1);
    }
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        EditorGUI.BeginProperty(position, label, property);
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;
        Rect foldoutRect = new Rect(position);
        //prevent our foldout from consuming all click events
        foldoutRect.height = EditorGUIUtility.singleLineHeight;
        // Draw label
        property.isExpanded = EditorGUI.Foldout(foldoutRect, property.isExpanded, label);
        if (property.isExpanded) {
            StatWrapperFloat wrapper = (StatWrapperFloat)fieldInfo.GetValue(property.serializedObject.targetObject);
            string[] names = wrapper.names;
            string suffix = wrapper.suffix;
            SerializedProperty dat = property.FindPropertyRelative("data");
            for (int i = 0; i < wrapper.Length; i++) {
                Rect r = new Rect(position.x, position.y + ITEM_HEIGHT * (i + 1), position.width, 20);
                GUIContent c = new GUIContent(names[i] + suffix);
                SerializedProperty p = dat.GetArrayElementAtIndex(i);
                EditorGUI.PropertyField(r, p, c);
            }
        }
        EditorGUI.indentLevel = indent;
        EditorGUI.EndProperty();
    }
}

[CustomPropertyDrawer(typeof(StatWrapperInt))]
class StatWrapperIntEditor : PropertyDrawer {
    public static readonly float ITEM_HEIGHT = 20;
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
        if (!property.isExpanded)
            return base.GetPropertyHeight(property, label); 
        StatWrapperInt wrapper = (StatWrapperInt)fieldInfo.GetValue(property.serializedObject.targetObject);
        float baseHeight = base.GetPropertyHeight(property, label);
        return ITEM_HEIGHT*(wrapper.Length + 1);
    }
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        EditorGUI.BeginProperty(position, label, property);
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;
        Rect foldoutRect = new Rect(position);
        //prevent our foldout from consuming all click events
        foldoutRect.height = EditorGUIUtility.singleLineHeight;
        // Draw label
        property.isExpanded = EditorGUI.Foldout(foldoutRect, property.isExpanded, label);
        if (property.isExpanded) { 
            StatWrapperInt wrapper = (StatWrapperInt)fieldInfo.GetValue(property.serializedObject.targetObject);
            string[] names = wrapper.names;
            string suffix = wrapper.suffix;
            SerializedProperty dat = property.FindPropertyRelative("data");
            for (int i = 0; i < wrapper.Length; i++) {
                Rect r = new Rect(position.x, position.y + ITEM_HEIGHT * (i + 1), position.width, 20);
                GUIContent c = new GUIContent(names[i] + suffix);
                SerializedProperty p = dat.GetArrayElementAtIndex(i);
                EditorGUI.PropertyField(r, p, c);
            }
        }
        EditorGUI.indentLevel = indent;
        EditorGUI.EndProperty();
    }
}
