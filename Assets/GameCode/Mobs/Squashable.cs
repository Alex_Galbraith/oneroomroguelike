﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Squashable : MonoBehaviour
{
    public abstract Vector2 Velocity { get; }
    public abstract Vector2 Accelleration { get; }
    public abstract bool Grounded { get; }
    public abstract bool FacingRight { get; }
    public abstract float SquashVert { get; }
}
