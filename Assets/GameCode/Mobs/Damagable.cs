﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamagable
{
    float DealDamage(Mob attacker, float amount, DamageElement element, DamageMethod method);
}
