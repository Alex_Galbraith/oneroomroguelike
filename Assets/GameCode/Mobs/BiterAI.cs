﻿//#define _DEBUG
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Simple2DController.ControllerState;
using static PathFinder;
using static PathFindingGrid.AStarType;
public class BiterAI : Squashable
{
    [Header("Path Finder")]
    public PathFindingScheduler PathFinder;
    public PathFindingGrid      PathGrid;
    public Simple2DController   Controller;
    public float                PathfindingCooldown = 1;

    [Header("Visuals")]
    public Animator             Animator;
    public SpriteRenderer       mRenderer;

    [Header("Target")]
    public Transform Target;

    [Header("Movement")]
    public float        Gravity = 80;
    public float        maxWalkSpeed = 5;
    public int          jumpHeight = 3;
    public float        knockbackDecay = 0.9f;
    private float       endKnockbackThreshold = 0.2f;

    [Header("Crowding control")]
    public float            nearbyRadius = 0.3f;
    public ContactFilter2D  nearbyMask;
    Collider2D[]            nearby = new Collider2D[3];

    [Header("Bite settings")]
    public LayerMask    BiteMask;
    public float        BiteRadius = 0.3f;
    public float        BiteAttemptDist = 0.4f;
    public bool         DoLunge = true;
    public float        lungeSpeed = 1;
    public float        biteSpeed = 1;
    private bool        Biting = false;

    public override Vector2 Velocity => velocity;
    public override Vector2 Accelleration => accelleration;
    public override bool Grounded => Controller.Grounded;
    public override bool FacingRight => true;
    public override float SquashVert => 1;

    private Instruction[]   instructions = new Instruction[32];
    private Instruction     i;
    private Instruction     lastI;
    
    private bool            PathFindingSet = false;
    private bool            PathFindingInProgress = false;
    private int             pathFindingProgress = 0;
    private int             pathCount;
    private int             lastProgress;
    private float           lastPathfindTime = 0;
    private Vector3         pathTarget;
    
    private float   jumpVel;
    private float   jvelx, jvely;
    private float   rootG;
    private bool    Jumping = false;
    private float   nodeDist = 0;

    private new Collider2D collider;

    private Vector2 velocity;
    private Vector2 lastVel;
    private Vector2 accelleration;

    public void KnockBack(Vector2 velocity, float time, bool force = false) {
        KnockedTime = time;
        if (force)
            this.velocity = velocity;
        else
            this.velocity += velocity;
        KnockedBack = true;
        PathFindingSet = false;
    }

    public bool KnockedBack {
        get;
        private set;
    }
    public float KnockedTime {
        get;
        private set;
    }

    

    public void PathFindingCallback(PathFinder.PathFindingResult result, int count) {
        //slightly randomize cooldown to prevent many simultaneous pathfind queues
        lastPathfindTime = Time.unscaledTime + (Random.value * .05f * PathfindingCooldown);
        PathFindingInProgress = false;
        
        if (result == PathFindingResult.DROPPED || result == PathFindingResult.INVALID || count == 0) {
            PathFindingSet = false;
            pathCount = count;
            pathFindingProgress = 0;
        }
        else {
            PathFindingSet = true;
            pathFindingProgress = count - 1;
            if (count > 1) {
                i = instructions[count-1];
                //can we continue where we left off?
                if (lastI.posx == i.posx && lastI.posy == i.posy) {
                    pathFindingProgress -= 1;
                }
            }
            pathCount = count;
        }

    }
    
    // Start is called before the first frame update
    void Start()
    {
        rootG = Mathf.Sqrt(Gravity * 1f);
        jumpVel =  Mathf.Sqrt(2*jumpHeight) * rootG;
        collider = GetComponent<Collider2D>();
        Animator.SetFloat("BiteSpeed", biteSpeed);
    }

    public void BiteFinished() {
        Biting = false;
        Animator.SetBool("Biting", false);
        PathFindingSet = false;
        lastPathfindTime = 0;
        Collider2D hit = Physics2D.OverlapCircle(transform.position, BiteRadius, BiteMask);
        CollisionUtils.GetMobClasses(hit, out IDamagable damagable , out _ );
        damagable?.DealDamage(GetComponent<Mob>(), 0.5f, DamageElement.PHYSICAL, DamageMethod.MELEE);
    }
    public void Lunge() {
        
        if (Biting && DoLunge) {
            Vector2 dir = (Vector2)(Target.position - transform.position);
            if (dir.sqrMagnitude > BiteAttemptDist * BiteAttemptDist) { 
                velocity = ((Vector2)(Target.position - transform.position)).normalized * lungeSpeed;
            }
        }
    }

    private bool CalculateJump(float dx, float dy, out float velX, out float velY) {
        dx -= Mathf.Sign(dx) * 0.5f;
        if (Mathf.Floor(dy) > jumpHeight) {
            velX = 0;
            velY = 0;
            return false;
        }
        float minTx = Mathf.Abs(dx) / maxWalkSpeed;
        float minTy = Mathf.Sqrt(2 * Mathf.Abs(dy)) / (rootG);
        float t = Mathf.Max(minTx, minTy);
        velX = dx / t;
        velY = Gravity *1f * t / 2 + dy / t;
        return true;
    }

    

    // Update is called once per frame
    void FixedUpdate()
    {
        //Apply gravity
        if (!Controller.Grounded)
            velocity += Vector2.down * Time.fixedDeltaTime * Gravity;

        float sqTargetDistance = ((Vector2)(transform.position - Target.transform.position)).sqrMagnitude;
        Vector3Int gridPos = PathGrid.WorldPosToDataCell(Controller.GroundedPos);
        if (!PathFindingInProgress && Controller.Grounded && !KnockedBack) {
            if (PathfindingCooldown <  Time.unscaledTime - lastPathfindTime) {
                PathFindingInProgress = true;
                Vector3 wpos = Controller.GroundedPos;
                if ((PathGrid.grid[gridPos.x][gridPos.y] & WALKABLE) == 0)
                    wpos.y += 1;
                PathFinder.PathFind(wpos, Target.position, WALKABLE, PathFindingCallback, instructions, true, jumpHeight, maxWalkSpeed, Gravity);
            }
        }
        Vector3 ptarget;
        
        i = new Instruction();
        if (PathFindingSet && sqTargetDistance>1) {
            //Skip walk nodes
            while (pathFindingProgress > 0) {
                i = instructions[pathFindingProgress-1];
                if (!i.nodeTransition && !instructions[pathFindingProgress].nodeTransition) { 
                    if (i.posy - gridPos.y <= 0 && instructions[pathFindingProgress].posy - gridPos.y <= 0) {
                        pathFindingProgress--;
                        continue;
                    }
                }
                break;
            }
            //If we've run out of instructions, set our target position to Target position
            //Otherwise set our target position to the next node position
            i = instructions[pathFindingProgress];
            if (pathFindingProgress == 0)
                ptarget = Target.position;
            else
                ptarget = PathGrid.DataCellToWorldPos(i.posx, i.posy);

        }
        else if (sqTargetDistance <= 1){
            ptarget = Target.position;
        }
        else {
            ptarget = transform.position;
        }

#if _DEBUG
        #region DEBUG
        for (int j = 0; j < pathFindingProgress; j++) {
            if (instructions[j].nodeTransition)
                Debug.DrawLine(PathGrid.DataCellToWorldPos(instructions[j].posx, instructions[j].posy), PathGrid.DataCellToWorldPos(instructions[j + 1].posx, instructions[j + 1].posy), new Color(255, 165, 0));
            else
                Debug.DrawLine(PathGrid.DataCellToWorldPos(instructions[j].posx, instructions[j].posy), PathGrid.DataCellToWorldPos(instructions[j + 1].posx, instructions[j + 1].posy), Color.red);
        }
        for (int j = pathFindingProgress; j < pathCount-1; j++) {

            if(instructions[j].nodeTransition)
                Debug.DrawLine(PathGrid.DataCellToWorldPos(instructions[j].posx, instructions[j].posy), PathGrid.DataCellToWorldPos(instructions[j + 1].posx, instructions[j + 1].posy), Color.cyan);
            else
                Debug.DrawLine(PathGrid.DataCellToWorldPos(instructions[j].posx, instructions[j].posy), PathGrid.DataCellToWorldPos(instructions[j + 1].posx, instructions[j + 1].posy), Color.blue);
        }
        if (!i.nodeTransition)
            Debug.DrawLine(transform.position, ptarget, Color.green);
        else
            Debug.DrawLine(transform.position, ptarget, new Color(255, 165, 0));
        #endregion
#endif

        Vector2 toTarget = ptarget - transform.position;
        Vector2 tTDir = toTarget.normalized;
        if (Controller.Grounded && Velocity.y <= 0) {
            Jumping = false;
        }
        if (sqTargetDistance < BiteAttemptDist * BiteAttemptDist && Controller.Grounded) {
            Biting = true;
            Animator.SetTrigger("Bite");
            Animator.SetBool("Biting", true);
            velocity.x = 0;
        }
        if (Biting) {
            Controller.Move(velocity, out velocity);
            SetAnimatorVars();
            return;
        }
        

        
        

        if (!KnockedBack) { 
            ////////////////////////////
            /////Pathfinding movement
            ////////////////////////////
            Vector2 nodeDelta = transform.position - ptarget;
            nodeDist = (nodeDelta).sqrMagnitude;
            velocity.x *= 0.9f;

            //Slow down fast
            if (toTarget.x * velocity.x < 0)
                velocity.x *= 0.6f;

            //Accelerate towards target
            velocity.x += Mathf.Clamp((toTarget.x)/Time.fixedDeltaTime, -maxWalkSpeed, maxWalkSpeed)*0.1f;
            velocity.x = Mathf.Clamp(velocity.x * Time.fixedDeltaTime,-Mathf.Abs(toTarget.x), Mathf.Abs(toTarget.x)) / Time.fixedDeltaTime;

            //If there is a one block difference in height and no slope, make the transition a jump transition
            if ((Mathf.Abs(toTarget.y) > 1 && Mathf.Abs(toTarget.y) < 2)){
                RaycastHit2D[] hit = new RaycastHit2D[1];
                collider.Raycast(Vector2.right * Mathf.Sign(-nodeDelta.x), Controller.terrainFilter, hit, 1);
                if (!hit[0]) {
                    i.nodeTransition = true;
                }
                else {
                    if (Mathf.Abs(hit[0].normal.x) > 0.8f) {
                        i.nodeTransition = true;
                    }
                }
            }
            //If we are able to jump, jump
            if ((i.nodeTransition) && Controller.Grounded && PathFindingSet && !Jumping && nodeDist > 0.25f) {
                if (CalculateJump(toTarget.x, toTarget.y, out jvelx, out jvely)) {
                    velocity.y = jvely;
                    velocity.x = jvelx;
                    Jumping = true;
                    Controller.State = JUMPING;
                    Controller.CollideOneWay = false;
                }
                else {
                    //We messed something up, rewind a step
                    pathFindingProgress++;
                }
            
            }
            //Clamp our velocity to our jump velocity
            if (Jumping) {
                velocity.x = Mathf.Clamp(velocity.x,-Mathf.Abs(jvelx), Mathf.Abs(jvelx));
            }

            //If we are about to fall onto our target position, make sure we collide with one ways
            if (toTarget.y > -0.8f && toTarget.y <= 0 || (velocity.y > 0 && toTarget.y < 0.5f && toTarget.y > 0)) {
                Controller.CollideOneWay = true;
            }

            //Move away from nearby enemies
            int count = Physics2D.OverlapCircle(transform.position, nearbyRadius, nearbyMask, nearby);
            for (int j = 0; j < count && count>1; j++) {
                Vector2 dir = transform.position - nearby[j].transform.position;
                float d = Vector2.SqrMagnitude(dir);
                if (d == 0)
                    continue;
                velocity.x += Mathf.Sign(dir.x) * maxWalkSpeed * 0.1f;
            }
            velocity.x = Mathf.Clamp(velocity.x, -maxWalkSpeed, maxWalkSpeed);

            Controller.Move(velocity, out velocity);


            //If we are about to jump, make sure our jump from point is reset if we get repathed
            //If we dont do this, we might end up jumping from a position where we cant
            //jump up to the next node
            if (Controller.Grounded && !Jumping)
                if (i.nodeTransition) 
                    if (i.posy>=gridPos.y)
                        lastI = new Instruction();
            

            //If we are close to our target point, move to next target
            if (Mathf.Abs(nodeDelta.x) < 0.2f && gridPos.y == i.posy) {
                if (Controller.Grounded) {
                    if (pathFindingProgress > 0) {
                        pathFindingProgress--;
                        lastI = i;
                    }
                }
            }
            //Allow for standing on slope
            else { 
                if (Mathf.Abs(nodeDelta.x) < 0.2f) {
                    if (nodeDelta.y > -1f && nodeDelta.y < 0) {
                        if (Mathf.Abs(Controller.GroundedPos.y) % 1 > 0.1f && Controller.Grounded) {
                            if (pathFindingProgress > 0) {
                                pathFindingProgress--;
                                lastI = i;
                            }
                        }
                    }
                }
            }
            
            //////////////////////////////
            ////End Pathfinding movement
            //////////////////////////////
        }
        //We are knocked back
        else {
            velocity *= knockbackDecay;
            KnockedTime -= Time.fixedDeltaTime;
            if (KnockedTime <= 0)
                KnockedBack = false;
            if (KnockedBack) {
                velocity *= knockbackDecay;
                if (Vector2.SqrMagnitude(velocity) < endKnockbackThreshold)
                    KnockedBack = false;
            }
            Controller.Move(velocity, out velocity);
        }

        SetAnimatorVars();
        accelleration = velocity - lastVel;
        lastVel = velocity;
    }

    private void SetAnimatorVars() {
        Animator.SetFloat("Speed", Mathf.Abs(velocity.x));
        Animator.SetBool("Grounded", Controller.Grounded);
        if (velocity.x > 0)
            mRenderer.flipX = false;
        else if (velocity.x < 0)
            mRenderer.flipX = true;
    }
}
