﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Simple2DController.ControllerState;

/*
 * Author: Alex Galbraith 2019
 * 
 * This class provides a simple custom physics implementation for 2D platformers. 
 * This includes slope walking, stair stepping, sliding, and one way platforms.
 * No input management is provided.
 * This code may be buggy. 
 * 
 * You are free to copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, provided the following conditions are met:
 * 1. Credit to the original author is maintained in the source code.
 * 2. Credit to the original author is visible to users of the final project, e.g., in the credits menu.
 * 
 * DISCLAIMER:
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */

[RequireComponent(typeof(Collider2D), typeof(Rigidbody2D))]
public class Simple2DController : MonoBehaviour {
    public enum ControllerState {
        WALKING,
        SLIDING,
        JUMPING,
        FALLING
    }

    public ControllerState State = WALKING;

    [Header("Movement")]
    [Tooltip("If we move too fast, we begin sliding")]
    public float MaxSpeedBeforeSlide = 8;
    [Header("Stepping and Slopes")]
    [Tooltip("Can we step up stairs?")]
    public bool DoStepUp = true;
    [Tooltip("How high can these stairs be?")]
    public float MaxStepUp = 1;
    [Tooltip("When trying to step up, how far horizontally should we move the character?")]
    public float StepUpHorDelta = .1f;
    [Range(0, Mathf.PI), Tooltip("Max walkable slope in radians")]
    //55 degrees in radians default
    public float SlopeLimit = 0.959931f;

    [Header("Physics")]
    public float normalBias = 0.05f;
    public float drag = 0.01f;
    public float groundCheckDist = 0.1f;
    public float fallCheckDist = 1f;
    
    
    [Tooltip("How many times per frame do we allow the character to slide off collisions")]
    public uint moveIterations = 3;
    [Tooltip("Filter for colliding with terrain")]
    public ContactFilter2D terrainFilter;
    [Tooltip("Filter for colliding with one way terrain. Usually needs a layer mask and a normal mask.")]
    public ContactFilter2D oneWayFilter;
    public bool CollideOneWay = true;

    private Vector2 lastVel;
    internal Vector2 velocity;
    new private BoxCollider2D collider;
    new private Rigidbody2D rigidbody;

    public bool Jumping {
        get {
            return State == JUMPING;
        }
    }

    public bool Falling {
        get {
            return State == FALLING;
        }
    }
    bool crouching = false;
    public bool Crouching {
        get {
            return crouching;
        }
    }

    public Vector2 Accelleration {
        get;
        private set;
    }

    public bool Grounded {
        get;
        private set;
    }

    public Vector2 GroundedPos {
        get;
        private set;
    }

    public bool FacingRight {
        get;
        private set;
    }

    public float SlopeAngle {
        get;
        private set;
    }

    private float NormToAngle(Vector2 norm) {
        //Since we would be dotting against Vector2.Up, i.e., (0,1), we can just take the y component
        float dot = norm.normalized.y;
        return Mathf.Acos(dot);
    }

    // Start is called before the first frame update
    void Start()
    {
        collider = GetComponent<BoxCollider2D>();
        rigidbody = GetComponent<Rigidbody2D>();
    }

    bool StepUp(Vector2 remainingDelta) {
        Vector2 init = transform.position;
        CastAndMove(Vector2.up * MaxStepUp, Vector2.up * MaxStepUp, out _, out _, normalBias, false);
        CastAndMove(Vector2.right * StepUpHorDelta * Mathf.Sign(remainingDelta.x), Vector2.right * StepUpHorDelta * Mathf.Sign(remainingDelta.x), out _, out _, 0.05f, false);
        bool h = CastAndMove(Vector2.down * MaxStepUp, Vector2.down * MaxStepUp, out _, out _, out RaycastHit2D hit, normalBias, false);
        if (h) {
            if (transform.position.y- init.y > 0.1f && NormToAngle(hit.normal) < SlopeLimit) {
                return true;
            }
        }
        transform.position = init;
        return false;
    }

    bool CastAndMove(Vector2 velocity, Vector2 remainingDelta, out Vector2 newVelocity, out Vector2 newRemaining, float normalBias = 0.05f, bool stepup = true) {
        return CastAndMove(velocity, remainingDelta, out newVelocity, out newRemaining, out _, normalBias, stepup);
    }


    RaycastHit2D[] hits = new RaycastHit2D[1];
    RaycastHit2D[] hitsOW = new RaycastHit2D[3];

    /// <summary>
    /// Returns hits on both one way and terrain laters
    /// </summary>
    /// <param name="COW">Do collide one way</param>
    /// <param name="remainingDelta">Vector to cast in</param>
    /// <param name="hit">output hit</param>
    /// <returns></returns>
    bool GetHit(bool COW, Vector3 remainingDelta, out RaycastHit2D hit) {
        hits[0] = new RaycastHit2D();
        int hitCount = collider.Cast(remainingDelta, terrainFilter, hits, remainingDelta.magnitude);
        hit = hits[0];
        if (COW) {
            int hitCountOW = collider.Cast(remainingDelta, oneWayFilter, hitsOW, remainingDelta.magnitude);
            for (int i = 0; i<hitCountOW; i++) {
                if ((!hit || hitsOW[i].distance < hit.distance) && hitsOW[i].distance > 0) {
                    hit = hitsOW[i];
                    hitCount++;
                    //Hits are returned in order of distance anyway so we can break once we find one
                    break;
                }
            }
        }

        return hitCount > 0;
    }
    /// <summary>
    /// Move in the specified direction, modifying velocity if we hit something
    /// </summary>
    /// <param name="velocity">Current veloctity</param>
    /// <param name="remainingDelta">Movement to do in this frame</param>
    /// <param name="newVelocity">Output new velocity</param>
    /// <param name="newRemaining">Remaining movement this frame after moving</param>
    /// <param name="hit">Output hit</param>
    /// <param name="normalBias">How much should we adjust the final position by the normal direction of the hit</param>
    /// <param name="stepup">Are we allowing stepping up stairs?</param>
    /// <returns></returns>
    bool CastAndMove(Vector2 velocity, Vector2 remainingDelta, out Vector2 newVelocity, out Vector2 newRemaining, out RaycastHit2D hit, float normalBias = 0.05f, bool stepup = true) {
        //Set up one way platform collisions
        bool COW = CollideOneWay && !crouching;
        Vector3 opos = rigidbody.position = transform.position;
        bool didHit = GetHit(COW, remainingDelta, out hit);
        //If we didnt hit, just move
        if (!didHit) {
            transform.Translate(remainingDelta);
            newRemaining = Vector3.zero;
            newVelocity = velocity;
            return false;
        }
        //We did hit
        else {
            //Move the character
            transform.position = hit.centroid + normalBias * hit.normal;
            //Remove moved vel from remaining
            newRemaining = remainingDelta + (Vector2)(opos - transform.position);
            newVelocity = velocity - (Vector2.Dot(velocity, hit.normal) * hit.normal);
            //Try step up
            if (Mathf.Abs(newVelocity.x) < 0.01f && Mathf.Abs(velocity.x)>0.01f && stepup) {
                if (StepUp(remainingDelta)) {
                    newVelocity = velocity;
                    return true;
                }
            }
            //Make remaining velocity change when hitting something
            newRemaining = newRemaining - Vector2.Dot(newRemaining, hit.normal) * hit.normal;
            return true;
        }
    }


    
    private bool lastGrounded = false;
    // Update is called once per frame
    private void Update() {
    }

    /// <summary>
    /// Try move in the specified direction.
    /// </summary>
    /// <param name="velocity">Movement vector</param>
    /// <param name="newVelocity">After moving and potentially colliding, this is the new velocity vector</param>
    public void Move(Vector2 velocity, out Vector2 newVelocity) {
        Vector2 posn = transform.position;
        //How much movement do we have this frame
        Vector2 remainingDelta = velocity * Time.fixedDeltaTime;
        int iter = 0;
        //While we have velocity remaining this frame (and iterations left), try move.
        while (remainingDelta.sqrMagnitude > 0 && iter++ < moveIterations)
            CastAndMove(velocity, remainingDelta, out velocity, out remainingDelta, normalBias, DoStepUp);

        //Check grounded
        GetHit(CollideOneWay && !crouching, Vector2.down * fallCheckDist, out RaycastHit2D hit);

        Grounded = false;

        if (Jumping && velocity.y <= 0.1f)
            State = FALLING;


        //Set states and grounded status
        if (hit){
            if (hit.distance < groundCheckDist && !Jumping) {
                if ((SlopeAngle = NormToAngle(hit.normal)) < SlopeLimit) {
                    Grounded = true;
                    GroundedPos = hit.point;
                    if (Mathf.Abs(velocity.x) <=  MaxSpeedBeforeSlide && !crouching)
                        State = WALKING;
                    else
                        State = SLIDING;
                }
                else {
                    State = SLIDING;
                }
            }
            else {
                hit.normal = Vector2.up;
            }
        }
        else {
            hit.normal = Vector2.up;
        }
        
        if ((!hit || hit.distance > groundCheckDist) && !Jumping) {
            State = FALLING;
        }


        //Stop slope hopping
        if (lastGrounded && !Grounded && velocity.y > 0 && Mathf.Abs(velocity.x) < 0.1f && State != JUMPING)
            velocity.y = 0;
        if(lastGrounded) {
            CastAndMove(Vector2.down, Vector2.down * groundCheckDist, out _, out _, 0, false);
            //manual bias
            transform.position += Vector3.up * normalBias;
        }

        lastGrounded = Grounded;
        Accelleration = velocity - lastVel;
        lastVel = velocity;
        if (velocity.x > 0)
            FacingRight = true;
        else if (velocity.x < 0)
            FacingRight = false;
        newVelocity = velocity;
    }
    /// <summary>
    /// Sometimes the character cant uncrouch because they are trapped in a small space.
    /// This function will check for this and uncrouch if there is room, fixing the character's position.
    /// </summary>
    public void TryUnCrouch() {
        if (!crouching)
            return;
        RaycastHit2D[] hits = new RaycastHit2D[1];
        if (collider.Cast(Vector2.down, terrainFilter, hits, collider.size.y) > 0) {
            transform.position = hits[0].centroid + normalBias * hits[0].normal;
        }
        Crouch(false);
    }

    public void Crouch(bool crouch = true) {
        if (crouch) {
            if (!crouching) { 
                collider.size = new Vector2(collider.size.x, collider.size.y / 2);
                collider.offset = collider.offset + Vector2.up * collider.size.y/2;
                CastAndMove(Vector2.down, Vector2.down * collider.size.y, out _, out _, 0, false);
                //manual bias
                transform.position += Vector3.up * normalBias;
                crouching = true;
            }
        }
        else {
            if (crouching) {
                RaycastHit2D[] hits = new RaycastHit2D[1];
                //Check if we can uncrouch downways or if we should uncrouch upwards
                if (collider.Cast(Vector2.down, terrainFilter, hits, collider.size.y) == 0) {
                    collider.offset = collider.offset + Vector2.down * collider.size.y / 2;
                    collider.size = new Vector2(collider.size.x, collider.size.y * 2);

                    crouching = false;
                }else
                if (collider.Cast(Vector2.up, terrainFilter, hits, collider.size.y) == 0) {
                    transform.Translate(new Vector3(0, collider.size.y, 0));
                    collider.offset = collider.offset + Vector2.down * collider.size.y / 2;
                    collider.size = new Vector2(collider.size.x, collider.size.y * 2);
                    
                    crouching = false;
                }
            }
        }
    }
}
