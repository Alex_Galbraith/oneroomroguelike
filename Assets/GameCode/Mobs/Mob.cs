﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mob : MonoBehaviour, IDamagable {

    public MobData          data;
    public IMobController   controller;

    public delegate void DamageCallback(DamageEvent d);
    public delegate void DeathCallback(DeathEvent d);

    public event DamageCallback     OnPreDamage;
    public event DamageCallback     OnPostDamage;
    public event DeathCallback      OnPreDeath;
    public event DeathCallback      OnPostDeath;

    private bool isDead     = false;
    public bool IsDead()    => isDead;

    /// <summary>
    /// Deal damage to this mob
    /// </summary>
    /// <param name="mobfrom">Who is dealing the damage</param>
    /// <param name="value">Amount of damage</param>
    /// <param name="damageElement">Element of damage</param>
    /// <param name="method">Method of dealing damage</param>
    /// <returns></returns>
    public float DealDamage(Mob mobfrom, float value, DamageElement damageElement, DamageMethod method) {
        float damage = (value + data.mobStats[MobStats.DAMAGE_RECEIVED] + data.methodTaken[method])
            *StatsUtil.MultSpaceToLinearSpace(data.mobStatsMult[MobStats.DAMAGE_RECEIVED]+data.elementalDamageTakenMult[(int)damageElement] +data.methodTakenMult[(int)method]);
        DamageEvent d = new DamageEvent {
            cancelled = false,
            mob = this,
            dealer = mobfrom,
            damageType = damageElement,
            method = method,
            amount = damage
        };
        OnPreDamage?.Invoke(d);
        if (!d.cancelled) { 
            value = d.amount;
            float damageAfterArmor = Mathf.Max(0, value - data.Armor);
            data.Armor = Mathf.Max(0, data.Armor - value);
            data.Health -= damageAfterArmor;
            OnPostDamage?.Invoke(d);
            return value + Mathf.Min(0, data.Health);
        }
        //We died, run death events
        if (data.Health <= 0) {
            DeathEvent dt = new DeathEvent {
                cancelled = false,
                mob = this,
                dealer = mobfrom,
                damageType = damageElement,
                method = method,
                totalDamageAmount = d.amount
            };
            OnPreDeath?.Invoke(dt);
            if (!dt.cancelled) {
                isDead = true;
                OnPostDeath?.Invoke(dt);
            }
        }
        return d.amount;
    }
    /// <summary>
    /// Apply knockback to this mob's controller
    /// </summary>
    /// <param name="velocity">Velocity of this knockback</param>
    /// <param name="time">Duration of the knockback</param>
    /// <param name="force">Should this override current momentum?</param>
    public void KnockBack(Vector2 velocity, float time, bool force = true) {
        controller?.KnockBack(velocity, time, force);
    }
}


public class DamageEvent {
    /// <summary>
    /// Only effective for predamage callback, does nothing for damage and post damage callbacks.
    /// </summary>
    public bool cancelled;
    public Mob mob;
    public Mob dealer;
    /// <summary>
    /// Amount of damage dealt, this may excede remaining health
    /// </summary>
    public float amount;
    public DamageElement damageType;
    public DamageMethod method;
}

public class DeathEvent {
    /// <summary>
    /// Only effective for predamage callback, does nothing for damage and post damage callbacks.
    /// </summary>
    public bool cancelled;
    public Mob mob;
    public Mob dealer;
    /// <summary>
    /// Amount of damage dealt, this may have exceded remaining health
    /// </summary>
    public float totalDamageAmount;
    public DamageElement damageType;
    public DamageMethod method;
}


public struct StatusEffect {
    public int damage;
    public DamageElement type;
    public DamageMethod method;
    public delegate void OnExpire(StatusEffect effect, Mob manip);
}