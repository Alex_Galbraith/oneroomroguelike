﻿//#define _DEBUG
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Simple2DController.ControllerState;
using static PathFinder;
using static PathFindingGrid.AStarType;
public class WalkerAIBase : Squashable
{
    public PathFindingScheduler pathFinder;
    public PathFindingGrid pgrid;
    public Simple2DController Controller;

    private new Collider2D collider;

    private Vector2 velocity;
    private Vector2 lastVel;
    public override Vector2 Velocity {
        get {
            return velocity;
        }
    }

    private Vector2 accelleration;
    public override Vector2 Accelleration {
        get {
            return accelleration;
        }
        
    }

    public override bool FacingRight => true;

    public override float SquashVert => 1;

    public float Gravity = 80;
    public float maxWalkSpeed = 5;
    public int jumpHeight = 3;
    public Transform Target;
    public float PathfindingCooldown = 1;
    public float knockbackDecay = 0.9f;
    private float endKnockbackThreshold = 0.2f;
    public float nearbyRadius = 0.3f;
    public ContactFilter2D nearbyMask;

    private Instruction[] instructions = new Instruction[32];
    private bool PathFindingSet = false;
    private bool PathFindingInProgress = false;
    private int pathFindingProgress = 0;
    private float lastPathfindTime = 0;
    private Vector3 pathTarget;
    private float jumpVel;
    private int pathCount;
    private float rootG;

    public void KnockBack(Vector2 velocity, float time, bool force = false) {
        KnockedTime = time;
        if (force)
            this.velocity = velocity;
        else
            this.velocity += velocity;
        KnockedBack = true;
        PathFindingSet = false;
    }
    public bool KnockedBack {
        get;
        private set;
    }
    public float KnockedTime {
        get;
        private set;
    }

    public override bool Grounded => Controller.Grounded;

    public void PathFindingCallback(PathFinder.PathFindingResult result, int count) {
        //slightly randomize cooldown to prevent many simultaneous pathfind queues
        lastPathfindTime = Time.unscaledTime + (Random.value * .05f * PathfindingCooldown);
        PathFindingInProgress = false;
        
        if (result == PathFindingResult.DROPPED || result == PathFindingResult.INVALID || count == 0) {
            PathFindingSet = false;
            pathCount = count;
            pathFindingProgress = 0;
        }
        else {
            PathFindingSet = true;
            pathFindingProgress = count - 1;
            if (count > 1) {
                i = instructions[count-1];
                //can we continue where we left off?
                if (lastI.posx == i.posx && lastI.posy == i.posy) {
                    pathFindingProgress -= 1;
                }
            }
            pathCount = count;
        }

    }
    
    // Start is called before the first frame update
    void Start()
    {
        rootG = Mathf.Sqrt(Gravity * 0.7f);
        jumpVel =  Mathf.Sqrt(2*jumpHeight) * rootG;
        collider = GetComponent<Collider2D>();
    }

    private bool CalculateJump(float dx, float dy, out float velX, out float velY) {
        if (Mathf.Floor(dy) > jumpHeight) {
            velX = 0;
            velY = 0;
            return false;
        }
        dy = Mathf.Ceil(Mathf.Abs(dy))*Mathf.Sign(dy);
        float minTx = Mathf.Abs(dx) / maxWalkSpeed;
        float minTy = Mathf.Sqrt(2 * Mathf.Abs(dy)) / (rootG);
        float t = Mathf.Max(minTx, minTy);
        velX = dx / t;
        velY = Gravity *0.8f * t / 2 + dy / t;
        return true;
    }
    Instruction i;
    Instruction lastI;
    private int lastProgress;
    private float jvelx, jvely;
    private bool Jumping = false;
    private float nodeDist = 0;
    Collider2D[] nearby = new Collider2D[3];
    // Update is called once per frame
    void Update()
    {

        if (!Controller.Grounded)
            velocity += Vector2.down * Time.deltaTime * Gravity;
        Vector3Int gridPos = pgrid.WorldPosToDataCell(Controller.GroundedPos);
        if (!PathFindingInProgress && Controller.Grounded && !KnockedBack) {
            if (PathfindingCooldown <  Time.unscaledTime - lastPathfindTime) {
                PathFindingInProgress = true;
                Vector3 wpos = Controller.GroundedPos;
                if ((pgrid.grid[gridPos.x][gridPos.y] & WALKABLE) == 0)
                    wpos.y += 1;
                pathFinder.PathFind(wpos, Target.position, WALKABLE, PathFindingCallback, instructions, true, jumpHeight, maxWalkSpeed, Gravity);
            }
        }
        Vector3 ptarget;
        
        i = new Instruction();
        if (PathFindingSet) {
            while (pathFindingProgress > 0) {
                i = instructions[pathFindingProgress-1];
                if (!i.nodeTransition && !instructions[pathFindingProgress].nodeTransition) { 
                    if (i.posy - gridPos.y == 0 && instructions[pathFindingProgress].posy - gridPos.y == 0) {
                        pathFindingProgress--;
                        continue;
                    }
                }
                break;
            }
            i = instructions[pathFindingProgress];
            ptarget = pgrid.DataCellToWorldPos(i.posx, i.posy);

        }
        else {
            ptarget = transform.position;//Target.position;
        }
        
#if _DEBUG
        for (int j = 0; j < pathFindingProgress; j++) {
            if (instructions[j].nodeTransition)
                Debug.DrawLine(pgrid.DataCellToWorldPos(instructions[j].posx, instructions[j].posy), pgrid.DataCellToWorldPos(instructions[j + 1].posx, instructions[j + 1].posy), new Color(255, 165, 0));
            else
                Debug.DrawLine(pgrid.DataCellToWorldPos(instructions[j].posx, instructions[j].posy), pgrid.DataCellToWorldPos(instructions[j + 1].posx, instructions[j + 1].posy), Color.red);
        }
        for (int j = pathFindingProgress; j < pathCount-1; j++) {

            if(instructions[j].nodeTransition)
                Debug.DrawLine(pgrid.DataCellToWorldPos(instructions[j].posx, instructions[j].posy), pgrid.DataCellToWorldPos(instructions[j + 1].posx, instructions[j + 1].posy), Color.cyan);
            else
                Debug.DrawLine(pgrid.DataCellToWorldPos(instructions[j].posx, instructions[j].posy), pgrid.DataCellToWorldPos(instructions[j + 1].posx, instructions[j + 1].posy), Color.blue);
        }
        if (!i.nodeTransition)
            Debug.DrawLine(transform.position, ptarget, Color.green);
        else
            Debug.DrawLine(transform.position, ptarget, new Color(255, 165, 0));
#endif

        Vector2 toTarget = ptarget - transform.position;
        Vector2 tTDir = toTarget.normalized;
        if (Controller.Grounded && Velocity.y <= 0) {
            Jumping = false;
        }

        if (!KnockedBack) { 
            ////////////////////////////
            /////Pathfinding movement
            ////////////////////////////
            Vector2 nodeDelta = transform.position - ptarget;
            nodeDist = (nodeDelta).sqrMagnitude;
            velocity.x *= 0.9f;
            //Slow down fast
            if (toTarget.x * velocity.x < 0)
                velocity.x *= 0.6f;
            velocity.x += Mathf.Clamp((toTarget.x)/Time.fixedDeltaTime, -maxWalkSpeed, maxWalkSpeed)*0.1f;
            velocity.x = Mathf.Clamp(velocity.x * Time.fixedDeltaTime,-Mathf.Abs(toTarget.x), Mathf.Abs(toTarget.x)) / Time.fixedDeltaTime;
            if ((Mathf.Abs(toTarget.y) > 1 && Mathf.Abs(toTarget.y) < 2)){
                RaycastHit2D[] hit = new RaycastHit2D[1];
                collider.Raycast(Vector2.right * Mathf.Sign(-nodeDelta.x), Controller.terrainFilter, hit, 1);
                if (!hit[0]) {
                    i.nodeTransition = true;
                }
                else {
                    if (Mathf.Abs(hit[0].normal.x) > 0.8f) {
                        i.nodeTransition = true;
                    }
                }
            }
            if ((i.nodeTransition) && Controller.Grounded && PathFindingSet && !Jumping && nodeDist > 0.25f) {
                
                if (CalculateJump(toTarget.x, toTarget.y, out jvelx, out jvely)) {
                    velocity.y = jvely;
                    velocity.x = jvelx;
                    Jumping = true;
                    Controller.State = JUMPING;
                    Controller.CollideOneWay = false;
                }
                else {
                    //We messed something up, rewind a step
                    pathFindingProgress++;
                }
            
            }

            if (Jumping) {
                velocity.x = Mathf.Clamp(velocity.x,-Mathf.Abs(jvelx), Mathf.Abs(jvelx));
            }
            if (toTarget.y > -0.8f && toTarget.y <= 0) {
                Controller.CollideOneWay = true;
            }

            int count = Physics2D.OverlapCircle(transform.position, nearbyRadius, nearbyMask, nearby);
            for (int j = 0; j < count; j++) {
                Vector2 dir = transform.position - nearby[j].transform.position;
                float d = Vector2.SqrMagnitude(dir);
                if (d == 0)
                    continue;
                velocity.x += Mathf.Sign(dir.x) * maxWalkSpeed * 0.05f;
            }
            velocity.x = Mathf.Clamp(velocity.x, -maxWalkSpeed, maxWalkSpeed);

            Controller.Move(velocity, out velocity);




            if(Controller.Grounded && !Jumping)
                if (i.nodeTransition) 
                    if (i.posy>=gridPos.y)
                        lastI = new Instruction();
                
            if (Mathf.Abs(nodeDelta.x) < 0.2f && gridPos.y == i.posy) {
            
                if (Controller.Grounded) {
                    if (pathFindingProgress > 0) {
                        pathFindingProgress--;
                        lastI = i;
                    }
                    else {
                        //PathFindingSet = false;
                    }
                }
            }
            else { 
                if (Mathf.Abs(nodeDelta.x) < 0.2f) {
                    if (nodeDelta.y > -1f && nodeDelta.y < 0) {
                        if (Mathf.Abs(Controller.GroundedPos.y) % 1 > 0.1f && Controller.Grounded) {
                            if (pathFindingProgress > 0) {
                                pathFindingProgress--;
                                lastI = i;
                            }
                        }
                    }
                }
            }
            
            //////////////////////////////
            ////End Pathfinding movement
            //////////////////////////////
        }
        else {
            velocity *= knockbackDecay;
            KnockedTime -= Time.fixedDeltaTime;
            if (KnockedTime <= 0)
                KnockedBack = false;
            if (KnockedBack) {
                velocity *= knockbackDecay;
                if (Vector2.SqrMagnitude(velocity) < endKnockbackThreshold)
                    KnockedBack = false;
            }
            Controller.Move(velocity, out velocity);
        }
        
        accelleration = velocity - lastVel;
        lastVel = velocity;
    }
}
