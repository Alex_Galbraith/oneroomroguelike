﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Contains all data pertaining to a generic mob
/// </summary>
public class MobData : MonoBehaviour
{
    [SerializeField]
    private float health;
    public float Health {
        get { return health; }
        set { health = value; }
    }
    [SerializeField]
    private float armor;
    public float Armor {
        get { return armor; }
        set { armor = value; }
    }
    [SerializeField]
    private float mana;
    public float Mana {
        get { return mana; }
        set { mana = value; }
    }
    [SerializeField]
    public StatWrapperFloat mobStats = new StatWrapperFloat(typeof(MobStats));
    [SerializeField]
    public StatWrapperFloat mobStatsMult = new StatWrapperFloat(typeof(MobStats));

    #region elemental defensive
    [SerializeField]
    public StatWrapperFloat elementalDamageTakenMult = new StatWrapperFloat(typeof(DamageElement));
    #endregion

    #region method defensive
    [SerializeField]
    public StatWrapperFloat methodTakenMult = new StatWrapperFloat(typeof(DamageMethod));
    [SerializeField]
    public StatWrapperInt methodTaken = new StatWrapperInt(typeof(DamageMethod));
    #endregion

    #region offensive
    [SerializeField]
    public StatWrapperFloat weaponStats = new StatWrapperFloat(typeof(WeaponStats));
    [SerializeField]
    public StatWrapperFloat weaponStatsMult = new StatWrapperFloat(typeof(WeaponStats));
    [SerializeField]
    public StatWrapperFloat projectileStats = new StatWrapperFloat(typeof(ProjectileStats));
    [SerializeField]
    public StatWrapperFloat projectileStatsMult = new StatWrapperFloat(typeof(ProjectileStats));
    #endregion

    #region elemental offensive
    [SerializeField]
    public StatWrapperFloat elementalDealtMult = new StatWrapperFloat(typeof(DamageElement));
    #endregion

    #region method offensive
    [SerializeField]
    public StatWrapperFloat methodDealtMult = new StatWrapperFloat(typeof(DamageMethod));
    #endregion

    public Dictionary<string, float> customData = new Dictionary<string, float>();

    #region access utilites
    public float GetStat(MobStats stat) {
        return mobStats[(int)stat] * StatsUtil.MultSpaceToLinearSpace(mobStatsMult[(int)stat]);
    }
    #endregion
}


