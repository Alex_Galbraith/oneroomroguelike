﻿using UnityEngine;
using System.Collections;

public interface IMobController {
    /// <summary>
    /// Apply knockback to this controller
    /// </summary>
    /// <param name="velocity">Velocity of this knockback</param>
    /// <param name="time">Duration of the knockback</param>
    /// <param name="force">Should this override current momentum?</param>
    void KnockBack(Vector2 velocity, float time, bool force = false);
}
