﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquashAndStrech : MonoBehaviour
{
    public Squashable controller;
    public new SpriteRenderer renderer;
    public float scale = 1f;
    [Tooltip("How much of velocity is added to the squash")]
    public float velocityScale = 0.1f;
    [Min(1), Tooltip("Maximum stretch/squash scaling")]
    public float max = 2f;
    [Range(0,1)]
    public float smoothing = 0.9f;
    [Range(0, 1)]
    public float velocitySmoothing = 0.6f;
    

    public Vector2 Offset;
    public Vector2 GroundedOffset;

    MaterialPropertyBlock block;
    // Start is called before the first frame update
    void Start()
    {
        block = new MaterialPropertyBlock();
    }
    Vector2 smoothAcc;
    Vector2 smoothVel;
    // Update is called once per frame
    void Update()
    {
        Vector2 stretch = smoothAcc * scale;
        
        //Depending on which way the controller is facing, we need to flip our x stretch and flip our sprite.
        if (!controller.FacingRight) {
                renderer.flipX = true;
        }
        else {
                renderer.flipX = false;
        }
        
        //Smooth everything out
        smoothAcc = ((1f - smoothing) * controller.Accelleration) + smoothAcc * smoothing;
        smoothVel = ((1f - smoothing) * controller.Velocity) + smoothVel * velocitySmoothing;

        Vector2 vel = smoothVel * velocityScale;
        stretch += vel;
        float mag;
        //if we are decellerating, flip our magnitude
        if (Vector2.Dot(stretch, smoothVel) <= 0) {
            mag = 1/(1+stretch.magnitude);
        }
        else {
            mag = 1+stretch.magnitude;
        }
        //clamp our magnitude
        mag = Mathf.Clamp(mag, 1/max, max);
        //Calculate rotation matrix
        float angle = Mathf.Atan2(stretch.y, -stretch.x);
        Quaternion q = Quaternion.Euler(0, 0, angle/Mathf.PI * 180);
        Matrix4x4 r = Matrix4x4.Rotate(q);
        //Calculate translation matrix
        Matrix4x4 t = Matrix4x4.Translate(controller.Grounded?GroundedOffset:Offset);
        //Calculate rotation matrix inverse
        Matrix4x4 t_i = Matrix4x4.Translate(controller.Grounded ? -GroundedOffset : -Offset);
        //Calculate scale matrix
        Matrix4x4 s = Matrix4x4.Scale(new Vector2(mag,1/mag));
        //Create our complete stretch matrix
        //The transpose of a rotation matrix is the inverse of the same rotation matrix, thus r.transpose = r.inverse
        Matrix4x4 m = t_i * GetStretchMatrix(1, controller.SquashVert)*  r.transpose * s * r * t ;
        //Set our matrix in shader
        renderer.GetPropertyBlock(block);
        block.SetMatrix("_Deform_Matrix", m);
        renderer.SetPropertyBlock(block);
    }

    Matrix4x4 GetRotationMatrix(float angle) {
        float cs = Mathf.Cos(angle);
        float s = Mathf.Sin(angle);
        Matrix4x4 m = new Matrix4x4(
            new Vector4(cs,s,0,0),
            new Vector4(-s, cs, 0, 0),
            new Vector4(0, 0, 1, 0),
            new Vector4(0, 0, 0, 1)
            );
        return m;
    }

    Matrix4x4 GetTranslationMatrix(float x, float y) {
        Matrix4x4 m = new Matrix4x4(
            new Vector4(1, 0, 0, 0),
            new Vector4(0, 1, 0, 0),
            new Vector4(0, 0, 1, 0),
            new Vector4(x, y, 0, 1)
            );
        return m;
    }

    Matrix4x4 GetStretchMatrix(float x, float y) {
        return new Matrix4x4(
            new Vector4(x, 0, 0, 0),
            new Vector4(0, y, 0, 0),
            new Vector4(0, 0, 1, 0),
            new Vector4(0, 0, 0, 1)
            );
    }

}
