﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MapGrid : MonoBehaviour
{

    public LayerMask layers;

    private static MapGrid instance;
    public static MapGrid Instance {
        get {
            if (instance == null) { 
                Debug.LogWarning("No grid found, creating");
                GameObject g = new GameObject("MapGrid");
                instance = g.AddComponent<MapGrid>();
                instance.layers = LayerMask.NameToLayer("Everything");
                instance.Generate();
            }
            return instance;
        }
    }
    private BoundsInt bounds;
    public static BoundsInt Bounds {
        get {
            return Instance.bounds;
        }
    }
    int bminx, bminy;

    private Vector2Int size;
    public static Vector2Int Size {
        get {
            return Instance.size;
        }
    }

    private Vector3 cellSize;
    public static Vector3 CellSize {
        get {
            return Instance.cellSize;
        }
    }
    private Vector3 cellGap;
    public static Vector3 CellGap {
        get {
            return Instance.cellGap;
        }
    }
    private Vector3 cellTotSize;
    public static Vector3 CellTotSize {
        get {
            return Instance.cellTotSize;
        }
    }

    private void Awake() {
        GameObject.DontDestroyOnLoad(this.gameObject);
        if (instance == null) {
            instance = this;
        }
        else {
            Debug.LogError("Duplicate grid, destroying");
            Destroy(this);
            return;
        }
        Generate();
    }

    #region dynamics
    public Vector3Int _WorldPosToDataCell(Vector3 pos) {
        Vector3Int p = new Vector3Int(Mathf.FloorToInt(pos.x / cellTotSize.x), Mathf.FloorToInt(pos.y / cellTotSize.y), 0);
        p -= bounds.min;
        return p;
    }
    public Vector3 _DataCellToWorldPos(Vector3Int pos) {
        return _DataCellToWorldPos(pos.x, pos.y, pos.z);
    }
    public Vector3 _DataCellToWorldPos(int x, int y, int z = 0) {
        x += bminx;
        y += bminy;
        Vector3 p = new Vector3(x * cellTotSize.x + cellSize.x / 2f, y * cellTotSize.y + cellSize.y / 2f, z * cellTotSize.z + cellSize.z / 2f);
        return p;
    }
    public Vector3 _DataCellToWorldPos(Vector2Int pos) {
        return _DataCellToWorldPos(pos.x, pos.y, 0);
    }
    #endregion
    #region statics
    public static Vector3Int WorldPosToDataCell(Vector3 pos) {
        return Instance._WorldPosToDataCell(pos);
    }
    public static Vector3 DataCellToWorldPos(Vector3Int pos) {
        return DataCellToWorldPos(pos.x, pos.y, pos.z);
    }
    public static Vector3 DataCellToWorldPos(int x, int y, int z = 0) {
        return Instance._DataCellToWorldPos(x,y,z);
    }
    public static Vector3 DataCellToWorldPos(Vector2Int pos) {
        return DataCellToWorldPos(pos.x, pos.y, 0);
    }
    #endregion

    /// <summary>
    /// Sets up the grid boundaries.
    /// </summary>
    public void Generate() {
        Tilemap[] maps = GameObject.FindObjectsOfType<Tilemap>();
        bool set = false;
        bounds = new BoundsInt();
        for (int i = 0; i < maps.Length; i++) {
            if ((1<<maps[i].gameObject.layer & layers.value) != 0) {
                if (!set) {
                    bounds = maps[i].cellBounds;
                    cellSize = maps[i].layoutGrid.cellSize;
                    cellGap = maps[i].layoutGrid.cellGap;
                    cellTotSize = cellSize + cellGap;
                    set = true;
                    continue;
                }
                BoundsInt Obounds = maps[i].cellBounds;
                bounds.xMin = Mathf.Min(bounds.xMin, Obounds.xMin);
                bounds.yMin = Mathf.Min(bounds.yMin, Obounds.yMin);
                bounds.xMax = Mathf.Max(bounds.xMax, Obounds.xMax);
                bounds.yMax = Mathf.Max(bounds.yMax, Obounds.yMax);
            }
        }
        bminx = bounds.xMin;
        bminy = bounds.yMin;

        size = (Vector2Int)Bounds.size;
    }

    /// <summary>
    /// Sets up the grid boundaries.
    /// </summary>
    public void Generate(Tilemap[] maps) {
        bool set = false;
        bounds = new BoundsInt();
        for (int i = 0; i < maps.Length; i++) {
            if ((1 << maps[i].gameObject.layer & layers.value) != 0) {
                if (!set) {
                    bounds = maps[i].cellBounds;
                    cellSize = maps[i].layoutGrid.cellSize;
                    cellGap = maps[i].layoutGrid.cellGap;
                    cellTotSize = cellSize + cellGap;
                    set = true;
                    continue;
                }
                BoundsInt Obounds = maps[i].cellBounds;
                bounds.xMin = Mathf.Min(bounds.xMin, Obounds.xMin);
                bounds.yMin = Mathf.Min(bounds.yMin, Obounds.yMin);
                bounds.xMax = Mathf.Max(bounds.xMax, Obounds.xMax);
                bounds.yMax = Mathf.Max(bounds.yMax, Obounds.yMax);
            }
        }
        bminx = bounds.xMin;
        bminy = bounds.yMin;

        size = (Vector2Int)Bounds.size;
    }

}
