﻿//#define _DEBUG

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using static PathFindingGrid.AStarType;
public class PathFindingGrid : MonoBehaviour {
    public Tilemap Solid;
    public Tilemap OneWay;
    public Tilemap Hazard;
    internal Vector2Int size;

    BoundsInt bounds;
    int bminx, bminy;


    private uint nodeCount = 0;
    public bool SHOW_NODES = true;
    public bool SHOW_CONNECTION_COUNT = true;
    public bool SHOW_CONNECTIONS = true;
    public bool SHOW_CLEARANCE = true;
    public bool SHOW_HEIGHTS = false;
    public bool SHOW_MANHATTAN = false;
    public int MaxHorDist;
    public bool conservative;
    public bool ConnectSpider = true;
    private static readonly long TIMEOUT = 5000;


    public bool Locked {
        get;
        set;
    }

    public bool Generating {
        get;
        private set;
    }

    internal Vector3 DataCellToWorldPos(int posx, int posy) {
        return DataCellToWorldPos(new Vector3Int(posx, posy, 0));
    }

    #region Types
    [Flags]
    public enum AStarType { 
        NOTHING =           0,
        AIR =               0b000000001,
        SOLID =             0b000000010,

        GROUNDED =          0b000000100,
        DROPPABLE =         0b000001000,
        SPIDER_WALKABLE =   0b000010000,
        HAZARD =            0b000100000,

        WALKABLE =          GROUNDED | DROPPABLE,
    }
    #endregion

    internal AStarType[][] grid;
    internal ushort[][] heights;
    internal NavigationNode[][] nodes;

    private Vector3 cellSize;
    private Vector3 cellGap;
    private Vector3 cellTotSize;
    // Start is called before the first frame update
    void Start()
    {
        
        Generate();
    }

    // Update is called once per frame
    void Update()
    {
    }

    AStarType GetOneWayTile(Vector3Int pos) {
        if (OneWay.GetTile(pos) != null)
            return SOLID;
        return AIR;
    }

    AStarType GetSolidTile(Vector3Int pos) {
        if (Solid.GetTile(pos) != null)
            return SOLID;
        if (Hazard.GetTile(pos) != null)
            return HAZARD;
        return AIR;
    }

    AStarType GetGrid(int x, int y, AStarType def) {
        if (x < 0 || y < 0 || x >= size.x || y >= size.y)
            return def;
        return grid[x][y];
    }

    public Vector3Int WorldPosToDataCell(Vector3 pos) {
        Vector3Int p = new Vector3Int(Mathf.FloorToInt(pos.x/ cellTotSize.x), Mathf.FloorToInt(pos.y/ cellTotSize.y), 0);
        p -= bounds.min;
        return p;
    }
    public Vector3 DataCellToWorldPos(Vector3Int pos) {
        return DataCellToWorldPos(pos.x,pos.y,pos.z);
    }
    public Vector3 DataCellToWorldPos(int x, int y, int z = 0) {
        x += bminx;
        y += bminy;
        Vector3 p = new Vector3(x * cellTotSize.x + cellSize.x / 2f, y * cellTotSize.y + cellSize.y / 2f, z * cellTotSize.z + cellSize.z / 2f);
        return p;
    }

    public Vector3 DataCellToWorldPos(Vector2Int pos) {
        return DataCellToWorldPos(pos.x, pos.y, 0);
    }


    private HashSet<Vector4> connections = new HashSet<Vector4>();

    private void ConnectNodes(Vector3Int from, Vector3Int to, int height) {
        Vector4 pos = new Vector4(from.x, from.y, to.x, to.y);
        if (connections.Contains(pos)){
            return;
        }
        if (Mathf.Abs(from.x - to.x) > MaxHorDist)
            return;
        if (Mathf.Abs(from.x - to.x) < 2 && Mathf.Abs(from.y - to.y) < 2)
            return;
        connections.Add(pos);
        nodes[from.x][ from.y].ConnectTo(nodes[to.x][ to.y], true, nodeCount, Mathf.Min(heights[from.x][from.y],heights[to.x][to.y])-1);
        nodeCount += 2;
    }

    private T[][] Create2D<T>(int sx, int sy) {
        T[][] arr = new T[sx][];
        InitAll<T>(sy, arr);
        return arr;
    }
    private void InitAll<T>(int sy, T[][] arr) {
        for (int i = 0; i < arr.Length; i++) {
            arr[i] = new T[sy];
        }
    }

    void Generate() {
        System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
        sw.Start();
        while (Locked) {
            if (sw.ElapsedMilliseconds > TIMEOUT)
                throw new TimeoutException("Exceded PathFindingGrid generate lock timeout.");
        }
        sw.Restart();
        Generating = true;


        UnityEngine.Profiling.Profiler.BeginSample("Generate");
        connections.Clear();
        bounds = MapGrid.Bounds;
        size = MapGrid.Size;
        bminx = bounds.xMin;
        bminy = bounds.yMin;
        cellSize = MapGrid.CellSize;
        cellGap = MapGrid.CellGap;
        cellTotSize = MapGrid.CellTotSize;

        //Populate grid

        grid = Create2D<AStarType>(size.x,size.y);
        nodes = Create2D<NavigationNode>(size.x,size.y);
        heights = Create2D <ushort>(size.x, size.y);

        Vector3Int pos = Vector3Int.zero;
        //Flag ASTAR_TYPEs and set up nodes
        for (;pos.x < size.x; pos.x++) {
            for (pos.y = size.y - 1 ; pos.y >= 0; pos.y--) {
                nodes[pos.x][ pos.y] = new NavigationNode(pos);
                grid[pos.x][ pos.y] = GetSolidTile( pos + bounds.min );
                if ((grid[pos.x][ pos.y] & AIR) > 0) {
                    if(pos.y+1<size.y-1)
                        heights[pos.x][ pos.y] = (ushort)(heights[pos.x][ pos.y + 1] + 1);
                    if ((GetSolidTile( pos + bounds.min + Vector3Int.down ) & SOLID)>0) {
                        grid[pos.x][ pos.y] |= GROUNDED;
                    }else
                    if ((GetOneWayTile( pos + bounds.min + Vector3Int.down ) & SOLID) > 0) {
                        grid[pos.x][ pos.y] |= DROPPABLE;
                    }
                    else
                    if (ConnectSpider) {
                        if ((GetOneWayTile( pos + bounds.min + Vector3Int.left ) & SOLID) > 0) {
                            grid[pos.x][ pos.y] |= SPIDER_WALKABLE;
                        }
                        else if ((GetOneWayTile( pos + bounds.min + Vector3Int.right ) & SOLID) > 0) {
                            grid[pos.x][ pos.y] |= SPIDER_WALKABLE;
                        }
                        else if ((GetOneWayTile( pos + bounds.min + Vector3Int.up ) & SOLID) > 0) {
                            grid[pos.x][ pos.y] |= SPIDER_WALKABLE;
                        }
                        else if ((GetSolidTile( pos + bounds.min + Vector3Int.left ) & SOLID) > 0) {
                            grid[pos.x][ pos.y] |= SPIDER_WALKABLE;
                        }
                        else if ((GetSolidTile( pos + bounds.min + Vector3Int.right ) & SOLID) > 0) {
                            grid[pos.x][ pos.y] |= SPIDER_WALKABLE;
                        }
                        else if ((GetSolidTile( pos + bounds.min + Vector3Int.up ) & SOLID) > 0) {
                            grid[pos.x][ pos.y] |= SPIDER_WALKABLE;
                        }
                    }
                }
            }
        }

        //Mark special features and connect falling nodes
        pos = Vector3Int.zero;
        for (; pos.x < size.x; pos.x++) {
            pos.y = size.y-1;
            Vector3Int droppedFrom = Vector3Int.zero;
            bool dropping = false;
            for (; pos.y >=0; pos.y--) {
                //Dropping links
                if ((grid[pos.x][ pos.y] & DROPPABLE) != 0) {
                    //Start dropping link
                    if (!dropping) {
                        dropping = true;
                        droppedFrom = pos;
                    }
                    else {
                        ConnectNodes(droppedFrom, pos, droppedFrom.y - pos.y);
                        droppedFrom = pos;
                    }

                }
                else
                if ((grid[pos.x][ pos.y] & GROUNDED)!=0) {

                    if (dropping) {
                        ConnectNodes(droppedFrom, pos, droppedFrom.y - pos.y);
                        dropping = false;
                    }
                }
                else if((grid[pos.x][pos.y] & HAZARD) != 0)
                    dropping = false;
            }
        }
        //Generate jump links
        Vector3Int[] islands = new Vector3Int[size.x];
        int[] gapHeights = new int[size.x];
        for (int y = size.y-1; y>= 0; y--) {
            int c = GetIslands(new Vector3Int(0, y, 0), new Vector3Int(size.x - 1, y, 0), islands, gapHeights, AIR | WALKABLE);
            for (int i = 0; i<c; i++) {
                CastDown(islands[2 * i], islands[2 * i + 1], gapHeights[i], false, false, Vector3Int.zero);
                
            }
        }
        UnityEngine.Profiling.Profiler.EndSample();
        Debug.Log(sw.ElapsedMilliseconds);
        Generating = false;
    }

    private void GetIslands(Vector3Int left, Vector3Int right, Vector3Int[] islands, Vector3Int[] gaps, AStarType mask, out int i, out int g) {
        int count = islands.Length / 2;
        i = 0;
        g = 0;
        //Left is found
        bool L = false;
        bool Lg = false;
        int Lpos = left.x;
        int Lgpos = left.x;
        while (i < count) {

            if (((grid[left.x][ left.y] & mask) != 0)) {
                if (!L) {
                    L = true;
                    Lpos = left.x;
                }
                if (Lg) {
                    gaps[2 * g] = new Vector3Int(Lgpos, left.y, 0);
                    gaps[2 * g + 1] = new Vector3Int(left.x, left.y, 0) + Vector3Int.left;
                    g++;
                    Lg = false;
                }
            }
            else {
                if (!Lg) {
                    Lg = true;
                    Lgpos = left.x;
                }
                if (L) {
                    islands[2 * i] = new Vector3Int(Lpos, left.y, 0);
                    islands[2 * i + 1] = new Vector3Int(left.x, left.y, 0) + Vector3Int.left;
                    i++;
                    L = false;
                }

            }

            if (left.x == right.x) {
                if (L) {
                    islands[2 * i] = new Vector3Int(Lpos, left.y, 0);
                    islands[2 * i + 1] = new Vector3Int(left.x, left.y, 0);
                    i++;
                }
                if (Lg) {
                    gaps[2 * g] = new Vector3Int(Lgpos, left.y, 0);
                    gaps[2 * g + 1] = new Vector3Int(left.x, left.y, 0);
                    g++;
                }
                return;
            }
            left.x++;
        }
        Debug.LogError("RAN OUT OF SPACE DURING ISLAND COUNT");
        return;
    }

    private int GetIslands(Vector3Int left, Vector3Int right, Vector3Int[] islands, int[] heightsOut, AStarType mask, bool inverse = false) {
        int count = islands.Length / 2;
        int i = 0;
        //Left is found
        bool L = false;
        int Lpos = left.x;
        int height = 0;
        while (i < count) {

            if (((grid[left.x][ left.y] & mask) != 0) == !inverse) {
                if (!L) { 
                    L = true;
                    Lpos = left.x;
                    height = heights[left.x][ left.y];
                }
                else {
                    height = Mathf.Min(height, heights[left.x][ left.y]);
                }
            }
            else {
                if (L){
                    islands[2 * i] = new Vector3Int(Lpos, left.y, 0) ;
                    islands[2 * i + 1] = new Vector3Int(left.x, left.y, 0) + Vector3Int.left ;
                    heightsOut[i] = height;
                    i++;
                    L = false;
                }
                
            }
            if (left.x == right.x) {
                if (L) {
                    islands[2 * i] = new Vector3Int(Lpos, left.y, 0);
                    islands[2 * i + 1] = new Vector3Int(left.x, left.y, 0);
                    i++;
                }
                return i;
            }
            left.x++;
        }
        Debug.LogError("RAN OUT OF SPACE DURING ISLAND COUNT");
        return i;
    }

    public void CastDownRecurse(Vector3Int left, Vector3Int right, int height, bool hitLeft, bool hitRight, Vector3Int hit) {
        
        /*if (height == 3 || height == 8) {
            if (left.x > 0)
                left.x--;
            if (right.x < size.x - 1)
                right.x++;
        }*/
        left.y -= 1;
        right.y -= 1;
        if (left.y < 0)
            return;
        if(!hitLeft && !hitRight) {
            if (right.x - left.x <= 0)
                return;
        }
        else
            if (right.x - left.x < 0)
                return;
        CastDown(left, right, height + 1, hitLeft, hitRight, hit);
    }

    private void CastDown(Vector3Int left, Vector3Int right, int height, bool hitLeft, bool hitRight, Vector3Int hit) {

        while ((grid[left.x][left.y] & HAZARD) != 0 && left.x<=right.x)
            left.x++;
        while ((grid[right.x][right.y] & HAZARD) != 0 && left.x <= right.x)
            right.x--;
        if (left.x > right.x)
            return;

        bool hL = (grid[left.x][ left.y] & WALKABLE) != 0;
        bool hR = (grid[right.x][ right.y] & WALKABLE) != 0;
        
        //walk map one down, left to right
        //Check for left and right hits
        //get islands
        Vector3Int[] islands = new Vector3Int[(right.x - left.x) + 4];
        Vector3Int[] gaps = new Vector3Int[(right.x - left.x) + 4];
        int isCount;
        int gapCount;
        GetIslands(left, right, islands, gaps, WALKABLE | HAZARD, out isCount, out gapCount);

#if _DEBUG
        Debug.Log("---------------------");
        Debug.Log("L: " + left);
        Debug.Log("R: " + right);
        Debug.Log("H: " + height);
        Debug.Log("HR: " + hitRight + " HL: "+hitLeft);
        Debug.Log("Hit: " + hit);
        Debug.Log("HRT: " + hR + " HLT: " + hL);
        Debug.Log("Is: " + isCount);
        Debug.Log("gaps: " + gapCount);
#endif


        //We didnt hit anything, keep going
        if (isCount == 0)
            CastDownRecurse(left, right, height, hitLeft, hitRight, hit);
        //We havent hit anything yet
        if (!hitLeft && !hitRight) {
            if (gapCount < isCount) {
                //hit straight down onto flat
                if (gapCount == 0)
                    return;
                //We hit on both left and right sides, recast for each gap
                for (int i = 0; i< isCount-1; i++) {
                    ConnectNodes(islands[2 * i + 1], islands[2 * i + 2], height);
                    CastDownRecurse(gaps[2 * i], gaps[2 * i + 1], height, true, false, islands[2 * i + 1]);
                    CastDownRecurse(gaps[2 * i], gaps[2 * i+1], height, false, true, islands[2 * i + 2]);
                }
            //we hit on one side only, recast on that side
            }else if (gapCount == isCount) {
                if (hL) {
                    int i;
                    for (i = 0; i < isCount-1; i++) {
                            ConnectNodes(islands[2 * i + 1], islands[2 * i + 2], height);
                        CastDownRecurse(gaps[2 * i], gaps[2 * i + 1], height, true, false, islands[2 * i + 1]);
                        CastDownRecurse(gaps[2 * i], gaps[2 * i + 1], height, false, true, islands[2 * i + 2]);
                    }
                    CastDownRecurse(gaps[2 * i], gaps[2 * i + 1], height, true, false, islands[2 * i + 1]);
                }
                else {
                    int i;
                    for (i = 1; i < isCount; i++) {
                            ConnectNodes(islands[2 * i - 1], islands[2 * i], height);
                        CastDownRecurse(gaps[2 * i], gaps[2 * i + 1], height, true, false, islands[2 * i - 1]);
                        CastDownRecurse(gaps[2 * i], gaps[2 * i + 1], height, false, true, islands[2 * i ]);
                    }
                    CastDownRecurse(gaps[0], gaps[1], height, false, true, islands[0]);
                }
            }
            //We didnt hit on either side, we have a bunch of gaps, cast down for each gap.
            else {
                for (int i = 0; i<isCount; i++) {
                    if (i < isCount -1) {
                        ConnectNodes(islands[2 * i + 1], islands[2 * i + 2], height);
                    }
                    CastDownRecurse(gaps[2 * i], gaps[2 * i + 1], height, false, true, islands[2 * i]);
                    CastDownRecurse(gaps[2 * i + 2], gaps[2 * i + 3], height, true, false, islands[2 * i + 1]);
                }
            }
            return;
        }
        else {
            //Conservative ensures only the nearest connection is made on each side.
            //This also ensures each connection made is not blocked by anything
            int start = 0;
            int end = isCount;
            if (conservative) {
                if (hitRight) {
                    start = Mathf.Max(0, end-1);
                }
                if (hitLeft) {
                    end = Mathf.Min(isCount, 1);
                }
            }
            if (gapCount < isCount) {
                Vector3Int thisHitSide = hitLeft ? left : right;
                int add = hitLeft ? 0 : 1;
                //hit straight down onto flat
                if (gapCount == 0) {
                    ConnectNodes(thisHitSide, hit, height);
                    return;
                }

                for (int i = start; i < end; i++) {
                    Vector3Int pos = islands[2 * i + add];

                    ConnectNodes(pos, hit, height);
                    if (conservative)
                        if ((hitLeft && hL) || (hitRight && hR))
                            break;
                    CastDownRecurse(gaps[2 * i], gaps[2 * i + 1], height, hitLeft, hitRight, hit);
                }
            }
            else if (gapCount == isCount) {
                
                if (hL) {
                    int add = hitLeft ? 0 : 1;
                    int i;
                    Vector3Int pos;
                    for (i = start; i < end - 1; i++) {
                        pos = islands[2 * i + add];
                        ConnectNodes(pos, hit, height);
                        if (!conservative | !hitLeft)
                            CastDownRecurse(gaps[2 * i], gaps[2 * i + 1], height, hitLeft, hitRight, hit);
                    }
                    pos = islands[2 * i + add];
                    ConnectNodes(pos, hit, height);
                    if (!conservative | !hitLeft)
                        CastDownRecurse(gaps[2 * i], gaps[2 * i + 1], height, hitLeft, hitRight, hit);
                }
                else {
                    int add = hitLeft ? 0 : 1;
                    int i;
                    Vector3Int pos;
                    for (i = start + 1; i < end; i++) {
                        pos = islands[2 * i + add];
                        ConnectNodes(pos, hit, height);
                        if (!conservative | !hitRight)
                            CastDownRecurse(gaps[2 * i], gaps[2 * i + 1], height, hitLeft, hitRight, hit);
                    }
                    i = 0;
                    pos = islands[2 * i + add];
                    ConnectNodes(pos, hit, height);
                    if (!conservative | !hitRight)
                        CastDownRecurse(gaps[0], gaps[1], 0, hitLeft, hitRight, hit);
                }
            }
            else {
                int add = hitLeft ? 0 : 1;
                Vector3Int pos;
                for (int i = start; i < end; i++) {
                    pos = islands[2 * i + add];
                    ConnectNodes(pos, hit, height);
                    if (!conservative | hitLeft)
                        CastDownRecurse(gaps[2 * i], gaps[2 * i + 1], height, hitLeft, hitRight, hit);
                    if (!conservative | hitRight)
                        CastDownRecurse(gaps[2 * i + 2], gaps[2 * i + 3], height, hitLeft, hitRight, hit);
                }
            }
            return;
        }
        
    }

    internal class Connection {
        public int fromx,fromy,tox,toy;
        public uint ID;
        public short clearance;
        public short height;
        public short totHeight;
        public float sqrtTotHeight;
        public float sqrtClearance;
        public float sqrtHeight;
        public short manhatt;

        public Connection(Vector3Int from, Vector3Int to, uint iD, short clearance) {
            this.fromx = from.x;
            this.fromy = from.y;
            this.tox = to.x;
            this.toy = to.y;
            ID = iD;
            this.clearance = clearance;
            this.height = (short)(to.y-from.y);
            manhatt = (short)( Math.Abs(height) + Math.Abs(to.x - from.x));
            this.totHeight = (short)(Math.Abs(height) + clearance);
            this.sqrtTotHeight = Mathf.Sqrt(totHeight);
            this.sqrtClearance = Mathf.Sqrt(clearance);
            this.sqrtHeight = Mathf.Sqrt(Math.Abs(height));
        }
    }

    internal class NavigationNode {
        public readonly Vector3Int position;
        public List<Connection> connections = new List<Connection>();
        public NavigationNode(Vector3Int p) {
            position = p;
        }
        public void ConnectTo(NavigationNode to, bool bidirectional, uint ID, int clearance = 0) {
            ConnectInternal(to, ID, clearance);
            if (bidirectional)
                to.ConnectInternal(this, ID + 1, clearance);
        }
        private void ConnectInternal(NavigationNode to,  uint ID, int clearance) {
            Connection c = new Connection(position, to.position, ID, (short)clearance);
            connections.Add(c);
        }
    }
#if UNITY_EDITOR
    private void OnDrawGizmosSelected() {
        Vector3Int pos = Vector3Int.zero;
        for (; pos.x < size.x; pos.x++) {
            pos.y = 0;
            for (; pos.y < size.y; pos.y++) {
                var t = grid[pos.x][ pos.y];
                var n = nodes[pos.x][ pos.y];
                if (SHOW_NODES) {
                    if ((t & GROUNDED)>0) {
                        Gizmos.DrawIcon(Solid.GetCellCenterWorld(pos + bounds.min), "BlueCircle");
                    }
                    if ((t & DROPPABLE)> 0) {
                        Gizmos.DrawIcon(Solid.GetCellCenterWorld(pos + bounds.min), "GreenCircle");
                    }
                    if ((t & SPIDER_WALKABLE) > 0) {
                        Gizmos.DrawIcon(Solid.GetCellCenterWorld(pos + bounds.min), "OrangeCircle");
                    }
                }
                if (SHOW_CONNECTION_COUNT)
                    if (n.connections.Count>0)
                        UnityEditor.Handles.Label(Solid.GetCellCenterWorld(n.position + bounds.min) - Camera.main.transform.forward, "" + n.connections.Count);
                if (SHOW_HEIGHTS)
                    UnityEditor.Handles.Label(Solid.GetCellCenterWorld(n.position + bounds.min) - Camera.main.transform.forward, "" + heights[pos.x][ pos.y]);
                

                foreach (var c in n.connections) {
                    Gizmos.color = Color.green;
                    if (SHOW_CONNECTIONS)
                        Gizmos.DrawLine(Solid.GetCellCenterWorld(new Vector3Int(c.fromx,c.fromy,0) + bounds.min), Solid.GetCellCenterWorld(new Vector3Int(c.tox,c.toy,0) + bounds.min));
                    if (SHOW_CLEARANCE)
                        UnityEditor.Handles.Label((Solid.GetCellCenterWorld(new Vector3Int(c.fromx,c.fromy,0) + bounds.min)+ Solid.GetCellCenterWorld(new Vector3Int(c.tox,c.toy,0) + bounds.min))/2, "" + c.clearance);
                    if (SHOW_MANHATTAN)
                        UnityEditor.Handles.Label((Solid.GetCellCenterWorld(new Vector3Int(c.fromx,c.fromy,0) + bounds.min) + Solid.GetCellCenterWorld(new Vector3Int(c.tox,c.toy,0) + bounds.min)) / 2, "" + c.manhatt);
                }
                
            }
        }
    }
#endif
}
