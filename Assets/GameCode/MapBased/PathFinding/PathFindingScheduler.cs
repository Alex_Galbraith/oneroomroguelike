﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using static PathFinder;
using static PathFindingGrid;

public class PathFindingScheduler : MonoBehaviour {
    public delegate void PathFindingComplete(PathFindingResult result, int count);
    public PathFindingGrid PathFindingGrid;
    [Range(0, 32)]
    public int concurrentPathFinders = 16;
    private int[] average;
    private int averagePos = 0;
    private PathFinder[] pathFinders;
    private ConcurrentBag<ushort> availablePathFinders;
    private ConcurrentBag<CallbackJob> callbackJobs;
    private Queue<PathFindingJob> jobs;
    [SerializeField]
    private int availableThreads;
    public bool MultiThread = true;
    private struct PathFindingJob {
        public AStarType mask;
        public bool doLinks;
        public Vector3 from;
        public Vector3 to;
        public PathFindingComplete callback;
        public PathFinder.Instruction[] instructions;
        public int maxHeight;
        public float hspeed;
        public float gravity;

    }

    private struct CallbackJob {
        public PathFindingComplete callback;
        public int count;
        public PathFindingResult result;
    }

    // Start is called before the first frame update
    void Start()
    {
        jobs = new Queue<PathFindingJob>(concurrentPathFinders);
        availablePathFinders = new ConcurrentBag<ushort>();
        callbackJobs = new ConcurrentBag<CallbackJob>();
        average = new int[60];
        pathFinders = new PathFinder[concurrentPathFinders];
        for (uint i =0; i < concurrentPathFinders; i++) {
            pathFinders[i] = new PathFinder(PathFindingGrid, i);
            availablePathFinders.Add((ushort)i);
        }
    }

    public void PathFind(Vector3 from, Vector3 to, AStarType mask, PathFindingComplete callback, PathFinder.Instruction[] instructions, bool doLinks = true, int maxHeight = int.MaxValue, float hspeed = float.MaxValue, float gravity = 1f) {
        if(instructions == null) {
            Debug.LogError("Pathfinding instruction array null");
            return;        
        }
        PathFindingJob j = new PathFindingJob {
            from = from,
            to = to,
            mask = mask,
            doLinks = doLinks,
            callback = callback,
            instructions = instructions,
            maxHeight = maxHeight,
            hspeed = hspeed,
            gravity = gravity
        };
        jobs.Enqueue(j);
    }

    // Update is called once per frame
    void Update()
    {
        average[averagePos++] = jobs.Count;
        averagePos %= average.Length;
        availableThreads = availablePathFinders.Count;

        //If our grid is generating, we dont want to access data async, drop all jobs
        if (PathFindingGrid.Generating) {
            while(jobs.Count > concurrentPathFinders) {
                PathFindingJob job = jobs.Dequeue();
                job.callback(PathFindingResult.DROPPED, 0);
            }
            return;
        }

        float av = 0;
        for (int i = 0; i < average.Length; i++) {
            av += average[i] / (float)average.Length;
        }
        //we cant keep up with demand so drop oldest
        for (int i = 0; i < av / concurrentPathFinders && jobs.Count > concurrentPathFinders; i++) {
            PathFindingJob job = jobs.Dequeue();
            job.callback(PathFindingResult.DROPPED, 0);
            Debug.LogWarning("Too many pathfinding jobs, dropping oldest jobs.");
        }

        while (jobs.Count > 0 && availablePathFinders.Count > 0) {
            bool success = availablePathFinders.TryTake(out ushort id);
            if (success) {
                PathFindingJob job = jobs.Dequeue();
                if (MultiThread)
                    Task.Factory.StartNew(() => PathFind(job, id));
                else
                    PathFind(job, id);
            }   
        }

        if (availablePathFinders.Count == concurrentPathFinders) {
            PathFindingGrid.Locked = false;
        }
        else {
            PathFindingGrid.Locked = true;
        }

        while (callbackJobs.Count>0) {
            CallbackJob j;
            if (callbackJobs.TryTake(out j))
                j.callback(j.result, j.count);
        }
    }

    private void PathFind(PathFindingJob j, ushort pathFinderID) {
        try { 
            PathFindingResult r = pathFinders[pathFinderID].Pathfind(j.instructions, j.from, j.to, j.mask, out int count, j.doLinks, j.maxHeight, j.hspeed, j.gravity);
            callbackJobs.Add(new CallbackJob {
                callback = j.callback,
                result = r,
                count = count
            });
            availablePathFinders.Add(pathFinderID);
        }
        catch(Exception e) {
            Debug.LogError(e);
        }
        
    }
}
