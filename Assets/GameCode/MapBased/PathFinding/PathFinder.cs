﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static PathFindingGrid;
using System.Linq;
using Node = PathFindingGrid.NavigationNode;
using System;

public class PathFinder {
    public PathFindingGrid grid;
    public int MaxInstructions = 64;
    public int InstructionCacheSize = 512;
    public uint ID;

    private static float[] rootTable;
    Node[][] nodes;
    AStarType[][] data;
    byte[][] closed;
    private void ClearClosed() {
        int mx = sizex;
        int my = sizey;
        for (int x = 0; x < mx; x++)
            for (int y = 0; y < my; y++)
                closed[x][y] = 255;
    }
    Vector2Int size;
    public struct Instruction {
        public uint ID;
        public int posx;
        public int posy;
        public bool nodeTransition;
        public uint toNode;
        public int value;
        public float heur;

        public override string ToString() {
            return "Instruction:\r\nID: " + ID + "\r\nX: " + posx + "\r\nY: " + posy + "\r\ntransition: " + nodeTransition + "\r\nvalue: " + value + "\r\nheur: " + heur;
        }
    }
    private int sizex, sizey;
    private Instruction[] instructionCache;
    public PathFinder(PathFindingGrid grid, uint ID) {
        this.grid = grid;
        instructionCache = new Instruction[InstructionCacheSize];
        this.ID = ID;

        if (grid == null) {
            Debug.LogError("No grid set");
            return;
        }
        nodes = grid.nodes;
        data = grid.grid;
        size = grid.size;
        sizex = size.x;
        sizey = size.y;
        rootTable = new float[sizey];
        for (int i = 0; i< sizey; i++) {
            rootTable[i] = (float)System.Math.Sqrt(i);
        }
        openSet = new SortedList<float, uint>(MaxInstructions);
        closed = Create2D<byte>(sizex, sizey);
    }
    float counter = 0;
    private float Heuristic(int x1, int y1, int x2, int y2) {
        counter += 0.0001f;
        
        return (System.Math.Abs(x1 - x2) + System.Math.Abs(y1 - y2)) * 1.01f + counter;
    }

    private T[][] Create2D<T>(int sx, int sy) {
        T[][] arr = new T[sx][];
        InitAll<T>(sy, arr);
        return arr;
    }
    private void InitAll<T>(int sy, T[][] arr) {
        for (int i = 0; i < arr.Length; i++) {
            arr[i] = new T[sy];
        }
    }

    private bool ValidatePos(int x, int y, AStarType mask) {
        if (x < 0 || y < 0 || x >= sizex || y >= sizey)
            return false;
        if ((mask & data[x][y]) == 0)
            return false;
        return true;
    }

    private bool ValidateLink(int fx, int tx, int height, int maxHeight, float sqrtJump, float hspeed, float invRootGrav, float sqrtClear, float sqrtTotHeight) {
        if (height > maxHeight)
            return false;
        if (Math.Abs(tx - fx) > GetMaxDistance(height, sqrtClear, sqrtTotHeight, invRootGrav, hspeed, sqrtJump, maxHeight))
            return false;
        return true;
    }

    private float GetMaxDistance(int cheight, float sqrtClear, float sqrtTotHeight, float invSqrtGrav, float hspeed, float sqrtJump, int jump) {
        if (cheight > 0) {
            return 1.414f * hspeed * (Math.Min(sqrtJump, sqrtTotHeight) + Math.Min(rootTable[jump - cheight], sqrtClear))* invSqrtGrav;
        }else if (cheight < 0) {
            return 1.414f * hspeed * (Math.Min(rootTable[jump - cheight], sqrtTotHeight) + Math.Min(sqrtJump, sqrtClear)) * invSqrtGrav;
        }
        else {
            return 2 * hspeed * (Math.Min(sqrtJump, sqrtTotHeight)) * invSqrtGrav;
        }
    }

    private SortedList<float, uint> openSet;
    private List<Vector2Int> closedSet = new List<Vector2Int>();
    private Dictionary<uint, uint> cameFrom = new Dictionary<uint, uint>();

    private uint currentID;
    private void AddNode(int x, int y, uint fromID, AStarType mask, int value) {
        if (ValidatePos(x, y, mask)) {
            if (closed[x][y] > value) {
                
                float h = Heuristic(x, y, tox, toy);
                
                openSet.Add(-value - h, currentID);
                
                closed[x][y] = (byte)value;
                cameFrom[currentID] = fromID;
                instructionCache[currentID] = new Instruction {
                    posx = x,
                    posy = y,
                    nodeTransition = false,
                    ID = currentID++,
                    toNode = 0,
                    value = value,
                    heur = h
                };
                
            }
        }
    }
    private void AddLink(Connection c, uint fromID, int maxHeight, int value, float sqrtJump, float hspeed, float invRootGrav) {
        int x = c.tox;
        int y = c.toy;
        if (closed[x][y] > value) {
            if (ValidateLink(c.fromx,x,c.height,maxHeight, sqrtJump, hspeed, invRootGrav, c.sqrtClearance, c.sqrtTotHeight)) {
                closed[x][y] = (byte)value;
                float h = Heuristic(x, y, tox, toy);
                openSet.Add(-value - h, currentID);
                cameFrom[currentID] = fromID;
                instructionCache[currentID] = new Instruction {
                    posx = x,
                    posy = y,
                    nodeTransition = true,
                    ID = currentID++,
                    toNode = c.ID,
                    value = value,
                    heur = h
                };
            }
        }
    }

    private static readonly Vector2Int up_right = new Vector2Int(1, 1);
    private static readonly Vector2Int up_left = new Vector2Int(1, -1);
    private void AddAll(int x, int y, uint fromID, int currentValue, AStarType mask, bool doLinks, int maxHeight, float sqrtJump, float hspeed, float invRootGrav) {
        closed[x][y] = (byte)currentValue;
        currentValue += 1;
        
        int x1 = x + 1;
        int y1 = y;
        AddNode( x1, y1, fromID, mask, currentValue);
        x1 = x - 1;
        y1 = y;
        AddNode(x1, y1, fromID, mask, currentValue);
        x1 = x;
        y1 = y + 1;
        AddNode(x1, y1, fromID, mask, currentValue);
        x1 = x;
        y1 = y - 1;
        AddNode(x1, y1, fromID, mask, currentValue);
        currentValue += 1;
        x1 = x - 1;
        y1 = y + 1;
        AddNode( x1, y1, fromID, mask, currentValue);
        x1 = x + 1;
        y1 = y + 1;
        AddNode(x1, y1, fromID, mask, currentValue);
        x1 = x + 1;
        y1 = y - 1;
        AddNode(x1, y1, fromID, mask, currentValue);
        x1 = x - 1;
        y1 = y - 1;
        AddNode(x1, y1, fromID, mask, currentValue);
        currentValue -= 2;
        
        if (doLinks) { 
            List<Connection> cons = nodes[x][y].connections;
            for(int i = 0; i < cons.Count; i++ ) {
                var c = cons[i];
                int manhatt = c.manhatt;
                AddLink(c, fromID, maxHeight, currentValue + manhatt, sqrtJump, hspeed, invRootGrav);
            }
        }
        
    }

    public PathFindingResult Pathfind(Instruction[] instructions, Vector3 from, Vector3 to, AStarType mask, out int count, bool doLinks, int maxHeight = int.MaxValue, float hspeed = float.MaxValue, float gravity = 1f) {
        Vector2Int npos = (Vector2Int)grid.WorldPosToDataCell(from);
        if (npos.x < 0 || npos.y < 0 || npos.x > sizex || npos.y > sizey) {
            count = -1;
            return PathFindingResult.INVALID;
        }
        Vector2Int npost = (Vector2Int)grid.WorldPosToDataCell(to);
        if (npost.x < 0 || npost.y < 0 || npost.x > npost.x || npost.y > sizey) {
            count = -1;
            return PathFindingResult.INVALID;
        }
        return Pathfind(instructions, npos, npost, mask, out count, doLinks, maxHeight,  hspeed, gravity);
    }

    public enum PathFindingResult {
        INVALID,
        NO_PATH,
        SUCCESS,
        DROPPED
    }
    private int tox, toy;
    public PathFindingResult Pathfind(Instruction[] instructions, Vector2Int from, Vector2Int to, AStarType mask, out int count, bool doLinks = true, int maxHeight = int.MaxValue, float hspeed = float.MaxValue, float gravity = 1f) {
        /*if (!ValidatePos(to.x, to.y, mask)) {
            count = -1;
            return PathFindingResult.INVALID;
        }*/
        try {
            float invRootGrav = 1f/(float)Math.Sqrt(gravity);
            float sqrtJump = (float)Math.Sqrt(maxHeight);
            counter = 0;
            currentID = 0;
            int minpos = 0;
            int sanityCheck = 0;
            closedSet.Clear();
            cameFrom.Clear();
            openSet.Clear();
            ClearClosed();
            tox = to.x;
            toy = to.y;
            int fx = from.x;
            int fy = from.y;
            Instruction next = new Instruction {
                posx = fx,
                posy = fy,
                nodeTransition = false,
                ID = currentID++,
                toNode = 0,
                value = 0,
                heur = Heuristic(fx, fy, tox, toy)
            };
            instructions[0] = next;
            instructionCache[next.ID] = next;
            AddAll(fx,fy, next.ID, 1, mask, doLinks, maxHeight, sqrtJump, hspeed, invRootGrav);
            PathFindingResult result = PathFindingResult.NO_PATH;
            Instruction min = next;
            float minHeur = 9001;
            while (next.value < MaxInstructions && openSet.Count > minpos) {
                if (next.posx == tox && next.posy == toy) {
                    result = PathFindingResult.SUCCESS;
                    break;
                }
                sanityCheck++;
                if (sanityCheck > 1000) {
                    Debug.LogError("Exceeded sanity check in pathfinding");
                    break;
                }
                next = instructionCache[openSet.Values[openSet.Count - 1]];
                openSet.RemoveAt(openSet.Count-1);
                if (minHeur > next.heur) {
                    min = next;
                    minHeur = next.heur;
                }
                AddAll(next.posx, next.posy, next.ID, next.value, mask, doLinks, maxHeight, sqrtJump, hspeed, invRootGrav);
            }
            next = min;
            sanityCheck = 0;
            int c = 0;
            int isize = instructions.Length;
        
            while (next.ID > 0) {
                sanityCheck++;
                if (sanityCheck > 1000) {
                    Debug.LogError("Exceeded sanity check in reconstruction");
                    break;
                }
                if (c < isize)
                    instructions[c++] = next;
                next = instructionCache[cameFrom[next.ID]];
            }
            if (c < isize)
                instructions[c++] = next;
            count = c;
            return result;
        }
        catch(Exception e) {
            Debug.LogError(e);
            count = 0;
            return PathFindingResult.DROPPED;
        }
        
    }
}
