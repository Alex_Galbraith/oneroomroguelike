﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : Spawnable {
    public SpawnType GetSpawnType => SpawnType.LASER;
    WeaponSlot slot;
    public WeaponSlot GetSlot => slot;
}
