﻿using UnityEngine;
using System.Collections;

public class BasicGun : Weapon {
    private WeaponSlot slot;

    public float delay = 0.1f;
    public float shotSpeed = 10f;
    public float shotCount = 1;
    public float accuracy = 0;
    public float maxAccuracy = 10;
    public float shotLifeSpan = 5;
    public float gravity = 0;
    public float spreadPerAccuracy = Mathf.PI/40;
    public float critChance = 0.1f;
    public float CritChanceDamageMult = 1.5f;
    public DamageElement primaryElement = 0;
    public int damage = 1;
    private float lastShot;
    

    public override void AddedToSlot(WeaponSlot slot) {
        this.slot = slot;
    }


    private float[] damages = new float[System.Enum.GetValues(typeof(DamageElement)).Length];
    public override void Held() {
        if (Time.time - lastShot > slot.GetStat(WeaponStats.COOLDOWN, delay)) {
            Vector3 pos = slot.player.transform.position;
            Vector2 mouseDir = ((Vector2)(Camera.main.ScreenToWorldPoint(Input.mousePosition) - pos)).normalized;
            slot.player.gameObject.GetComponent<PlatformerController>().KnockBack(-mouseDir * slot.GetStat(WeaponStats.SELF_KNOCKBACK), 0.5f, false);

            
            float _shotSpeed = slot.GetStat(ProjectileStats.SPEED,shotSpeed);
            float _shotCount = slot.GetStat(WeaponStats.SHOT_COUNT, shotCount);
            float _accuracy = Mathf.Max(0,(maxAccuracy- slot.GetStat(WeaponStats.ACCURACY, shotSpeed))/ maxAccuracy)* spreadPerAccuracy;
            SpawnEvent<Spawnable> s = new SpawnEvent<Spawnable> { cancelled = false, slot = slot, spawned = null };
            //Copy damage values
            for (int j = 0; j < damages.Length; j++) {
                damages[j] = slot.GetStat((DamageElement)j, j == (int)primaryElement ? damage + slot.methodDealt[0] : 0, slot.methodDealtMult[0] + slot.owner.data.methodDealtMult[0]);
                    
            }
            for (int i = 0; i < (int)(slot.GetStat(WeaponStats.SHOT_COUNT,shotCount)); i++) {
                s.cancelled = false;
                s.spawned = null;
                s.slot = slot;
                slot.callPreProjectileSpawn(s );
                if (!s.cancelled) {
                    Projectile p = ProjectilePool.GetProjectile();
                    if (p != null) {
                        p.gameObject.SetActive(true);
                        p.TriggerChild.SetActive(true);
                        p.transform.position = pos;
                        p.transform.localScale *= slot.GetStat(ProjectileStats.SIZE, 1f);
                        p.lifeSpan = slot.GetStat(ProjectileStats.LIFESPAN,shotLifeSpan);
                        p.Rigidbody.velocity = MathUtils.Rotate(mouseDir,_accuracy*(Random.value*2 -1)) * _shotSpeed;
                        p.Rigidbody.gravityScale = slot.GetStat(ProjectileStats.GRAVITY);
                        p.Rigidbody.drag = slot.GetStat(ProjectileStats.DRAG);
                        //Copy damage values
                        for (int j = 0; j < damages.Length; j++) {
                            p.ElementalDealt[j] = damages[j];
                        }
                        lastShot = Time.time;

                        p.slot = slot;

                        s.spawned = p;
                        slot.callPostProjectileSpawn(s);
                    }
                    //we are out of projectiles
                    else
                        break;
                }
            }
        }
    }

    public override void Pressed() {
    }

    public override void Released() {
    }

    public override void RemovedFromSlot() {
        slot = null;
    }
}
