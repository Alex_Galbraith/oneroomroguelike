﻿using UnityEngine;
using System.Collections;

public abstract class Weapon : ScriptableObject{
    public abstract void Held();
    public abstract void Pressed();
    public abstract void Released();
    public abstract void AddedToSlot(WeaponSlot slot);
    public abstract void RemovedFromSlot();
}
