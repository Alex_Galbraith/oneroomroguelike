﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSlot : ScriptableObject
{
    public Mob owner;
    public Player player;
    private Weapon weapon;
    public Weapon Weapon {
        get {
            return weapon;
        }
        set {
            weapon?.RemovedFromSlot();
            weapon = value;
            weapon?.AddedToSlot(this);
        }
    }

    public void Held() {
        weapon?.Held();
    }
    public void Pressed() {
        weapon?.Pressed();
    }
    public void Released() {
        weapon?.Released();
    }

    #region delegates
    public delegate void SpawnDelegate(SpawnEvent<Spawnable> e);

    #endregion

    #region events
    public event SpawnDelegate PreProjectileSpawn;
    public void callPreProjectileSpawn(SpawnEvent<Spawnable> e) {
        PreProjectileSpawn?.Invoke(e);
    }

    public event SpawnDelegate PostProjectileSpawn;
    public void callPostProjectileSpawn(SpawnEvent<Spawnable> e) {
        PostProjectileSpawn?.Invoke(e);
    }

    #endregion

    [SerializeField]
    public StatWrapperFloat weaponStats = new StatWrapperFloat(typeof(WeaponStats));
    [SerializeField]
    public StatWrapperFloat weaponStatsMult = new StatWrapperFloat(typeof(WeaponStats));

    [SerializeField]
    public StatWrapperFloat projectileStats = new StatWrapperFloat(typeof(ProjectileStats));
    [SerializeField]
    public StatWrapperFloat projectileStatsMult = new StatWrapperFloat(typeof(ProjectileStats));

    #region elemental offensive
    public StatWrapperFloat elementalDealtMult = new StatWrapperFloat(typeof(DamageElement));
    public StatWrapperFloat elementalDealt = new StatWrapperFloat(typeof(DamageElement));
    #endregion

    #region method offensive
    public StatWrapperFloat methodDealtMult = new StatWrapperFloat(typeof(DamageMethod));
    public StatWrapperFloat methodDealt = new StatWrapperFloat(typeof(DamageMethod));
    #endregion
    /// <summary>
    /// Get a weapon stat with extra additive and multiplicative values.
    /// </summary>
    /// <param name="stat">Stat to get</param>
    /// <param name="add">Linear amount to add. E.G., if the stat is normally 5 * 2 = 10, setting add = 1 will result in (5 + 1) * 2 = 12.</param>
    /// <param name="mult">Multiplicative amount to add in multiply space. E.G., if the stat is normally 5 * 2 = 10, setting mult = 1 will result in 5 * ( 2 + 1 ) = 15.</param>
    /// <returns></returns>
    public float GetStat(WeaponStats stat, float add = 0, float mult = 0) {
        return (weaponStats[(int)stat] + add + owner.data.weaponStats[(int)stat])
            * StatsUtil.MultSpaceToLinearSpace(weaponStatsMult[(int)stat] + mult + owner.data.weaponStatsMult[(int)stat]);
    }

    /// <summary>
    /// Get a projectile stat with extra additive and multiplicative values.
    /// </summary>
    /// <param name="stat">Stat to get</param>
    /// <param name="add">Linear amount to add. E.G., if the stat is normally 5 * 2 = 10, setting add = 1 will result in (5 + 1) * 2 = 12.</param>
    /// <param name="mult">Multiplicative amount to add in multiply space. E.G., if the stat is normally 5 * 2 = 10, setting mult = 1 will result in 5 * ( 2 + 1 ) = 15.</param>
    /// <returns></returns>
    public float GetStat(ProjectileStats stat, float add = 0, float mult = 0) {
        return (projectileStats[(int)stat] + add + owner.data.projectileStats[(int)stat])
            * StatsUtil.MultSpaceToLinearSpace(projectileStatsMult[(int)stat] + mult + owner.data.projectileStatsMult[(int)stat]);
    }

    /// <summary>
    /// Get a elemental dealt stat with extra additive and multiplicative values.
    /// </summary>
    /// <param name="stat">Stat to get</param>
    /// <param name="add">Linear amount to add. E.G., if the stat is normally 5 * 2 = 10, setting add = 1 will result in (5 + 1) * 2 = 12.</param>
    /// <param name="mult">Multiplicative amount to add in multiply space. E.G., if the stat is normally 5 * 2 = 10, setting mult = 1 will result in 5 * ( 2 + 1 ) = 15.</param>
    /// <returns></returns>
    public float GetStat(DamageElement stat, float add = 0, float mult = 0) {
        return (elementalDealt[(int)stat] + add)
            * StatsUtil.MultSpaceToLinearSpace(elementalDealtMult[(int)stat] + mult + owner.data.elementalDealtMult[(int)stat]);
    }

    /// <summary>
    /// Get a method dealt stat with extra additive and multiplicative values.
    /// </summary>
    /// <param name="stat">Stat to get</param>
    /// <param name="add">Linear amount to add. E.G., if the stat is normally 5 * 2 = 10, setting add = 1 will result in (5 + 1) * 2 = 12.</param>
    /// <param name="mult">Multiplicative amount to add in multiply space. E.G., if the stat is normally 5 * 2 = 10, setting mult = 1 will result in 5 * ( 2 + 1 ) = 15.</param>
    /// <returns></returns>
    public float GetStat(DamageMethod stat, float add = 0, float mult = 0) {
        return (methodDealt[(int)stat] + add)
            * StatsUtil.MultSpaceToLinearSpace(methodDealtMult[(int)stat] + mult + owner.data.methodDealtMult[(int)stat]);
    }
}

#region event classes
public class SpawnEvent<T> where T : Spawnable {
    public bool cancelled;
    public T spawned;
    public WeaponSlot slot;
}


#endregion  