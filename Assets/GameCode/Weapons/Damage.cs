﻿using UnityEngine;
using System.Collections;

public enum DamageElement {
    PHYSICAL,
    FIRE,
    ICE,
    ELECTRICITY,
    POISON,
    EXPLOSIVE,
    HEAL
}
public enum DamageMethod {
    MELEE,
    RANGED,
    REFLECTED,
    DOT
}