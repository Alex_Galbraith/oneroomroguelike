﻿using UnityEngine;
using System.Collections;

public interface Spawnable {
    SpawnType GetSpawnType {
        get;
    }
    WeaponSlot GetSlot {
        get;
    }
}
public enum SpawnType {
    PROJECTILE,
    LASER,
    SUMMON,
    MELEE
}

public enum DestroyedReason {
    AGE,
    COLLISION,
    DAMAGE,
    OTHER
}
#region event classes
public class MobHitEvent<T> where T : Spawnable {
    public Vector2 hitPoint;
    public Vector2 normal;
    public Vector2 velocity;
    public Mob mobHit;
    public T dealer;
}

public class HitTerrainEvent<T> where T : Spawnable {
    public Vector2 hitPoint;
    public Vector2 normal;
    public Vector2 velocity;
    public T actor;
}
#endregion