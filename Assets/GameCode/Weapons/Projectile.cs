﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour, Spawnable {
    public SpawnType GetSpawnType => SpawnType.PROJECTILE;
    public WeaponSlot slot;
    public WeaponSlot GetSlot => slot;

    public int ID;

    #region delegates
    public delegate void HitEnemyDelegate(MobHitEvent<Projectile> e);
    public delegate void HitFriendlyDelegate(MobHitEvent<Projectile> e);
    public delegate void HitTerrainDelegate(HitTerrainEvent<Projectile> e);
    public delegate void UpdateDelegate(Projectile p, float deltaTime);
    public delegate void DestroyedDelegate(Projectile p, DestroyedReason reason);
    #endregion

    #region events
    public event HitEnemyDelegate PreEnemyHit;
    public event HitEnemyDelegate PostEnemyHit;

    public event HitFriendlyDelegate PreFriendlyHit;
    public event HitFriendlyDelegate PostFriendlyHit;

    public event HitTerrainDelegate PreTerrainHit;
    public event HitTerrainDelegate PostTerrainHit;

    public event UpdateDelegate UpdateEvent;

    public event DestroyedDelegate Destroyed;
    #endregion

    #region game object related
    public SpriteRenderer Renderer;
    public CircleCollider2D Collider;
    public Rigidbody2D Rigidbody;
    public GameObject TriggerChild;
    public CircleCollider2D Trigger;
    #endregion

    public float lifeSpan;
    public float knockBack;

    #region collision
    public int TerrainCollisionsBeforeDestruction;
    public int FriendlyCollisionsBeforeDestruction;
    public int EnemyCollisionsBeforeDestruction;

    public string FriendlyTag = "player";
    public string EnemyTag = "enemy";
    public string TerrainTag = "terrain";

    public int TickMultiple;
    #endregion

    #region all offensive
    public float[] ElementalDealt = new float[System.Enum.GetValues(typeof(DamageElement)).Length];
    public int[] MethodDealt = new int[System.Enum.GetValues(typeof(DamageMethod)).Length];

    public bool crit;
    #endregion
    /// <summary>
    /// Clean this projectile of all data so it can be spawned anew
    /// </summary>
    public void Clean() {
        crit = false;

        slot = null;

        Array.Clear(ElementalDealt,0,ElementalDealt.Length);
        Array.Clear(MethodDealt,0, MethodDealt.Length);

        Trigger.offset      = Collider.offset = Vector2.zero;
        Trigger.radius      = Collider.radius = 0.1f;
        Collider.isTrigger  = false;
        Trigger.isTrigger   = true;

        gameObject.layer    = 13;
        TriggerChild.layer  = 14;

        transform.localScale = Vector3.one;

        Rigidbody.gravityScale      = 0;
        Rigidbody.velocity          = Vector2.zero;
        Rigidbody.drag              = 0;
        Rigidbody.mass              = 0.01f;
        Rigidbody.angularDrag       = 0;
        Rigidbody.angularVelocity   = 0;

        //Clear event listeners
        PreEnemyHit         = null;
        PreFriendlyHit      = null;
        PreTerrainHit       = null;
        PostEnemyHit        = null;
        PostFriendlyHit     = null;
        PostTerrainHit      = null;
        UpdateEvent         = null;
        Destroyed           = null;

        slot = null;

        gameObject.SetActive(false);
        TriggerChild.SetActive(false);
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        var contact = collision.GetContact(0);
        Collided(collision.collider, contact.point, contact.normal, collision.relativeVelocity);
    }

    private void OnTriggerEnter2D(Collider2D collided) {
        
        Vector2 from = (Vector2)Trigger.transform.position + Trigger.offset - Rigidbody.velocity;
        Vector2 to = (Vector2)collided.transform.position + collided.offset;
        var hit = Physics2D.Raycast(from, (to - from), to.magnitude, 1 << collided.gameObject.layer);
        Vector2 point = hit.point;
        Vector2 normal = hit.normal;
        Vector2 velocity = Rigidbody.velocity;
        Collided(collided, point, normal, velocity);
    }

    private void OnTriggerStay2D(Collider2D collided) {
        if (TickMultiple <= 0)
            return;
        Vector2 from = (Vector2)Trigger.transform.position + Trigger.offset;
        Vector2 to = (Vector2)collided.transform.position + collided.offset;
        Vector2 point = (from + to)/2f;
        Vector2 normal = Vector2.zero;
        Vector2 velocity = Rigidbody.velocity;
        Collided(collided, point, normal, velocity);

    }

    private void Collided(Collider2D collided, Vector2 point, Vector2 normal, Vector2 velocity) {
        string tag = collided.tag.ToLower();
        if (tag.Equals(FriendlyTag)) {
            CollisionUtils.GetMobClasses(collided, out var damagable, out var mob);
            PreFriendlyHit?.Invoke(new MobHitEvent<Projectile> { dealer = this, mobHit = mob, hitPoint = point, normal = normal, velocity = velocity });
            if (mob) {
                for (int i = 0; i < ElementalDealt.Length; i++) {
                    if (!gameObject.activeSelf)
                        return;
                    mob.DealDamage(slot.owner, ElementalDealt[i], (DamageElement)i, DamageMethod.RANGED);
                }
            }
            if (FriendlyCollisionsBeforeDestruction-- <= 0) {
                if (!gameObject.activeSelf)
                    return;
                Destroyed?.Invoke(this, DestroyedReason.COLLISION);
                ProjectilePool.ReturnProjectile(this);
            }
        }
        else if (tag.Equals(EnemyTag)) {
            CollisionUtils.GetMobClasses(collided, out var damagable, out var mob);
            PreEnemyHit?.Invoke(new MobHitEvent<Projectile> { dealer = this, mobHit = mob, hitPoint = point, normal = normal, velocity = velocity });
            if (damagable != null) {
                for (int i = 0; i < ElementalDealt.Length; i++) {
                    if (!gameObject.activeSelf)
                        return;
                    damagable.DealDamage(slot.owner, ElementalDealt[i], (DamageElement)i, DamageMethod.RANGED);
                }
            }
            if (EnemyCollisionsBeforeDestruction-- <= 0) {
                if (!gameObject.activeSelf)
                    return;
                Destroyed?.Invoke(this, DestroyedReason.COLLISION);
                ProjectilePool.ReturnProjectile(this);
            }

        }
        else {
            PreTerrainHit?.Invoke(new HitTerrainEvent<Projectile> { actor = this, hitPoint = point, normal = normal, velocity = velocity });
            if (TerrainCollisionsBeforeDestruction-- <= 0) {
                if (!gameObject.activeSelf)
                    return;
                Destroyed?.Invoke(this, DestroyedReason.COLLISION);
                ProjectilePool.ReturnProjectile(this);
            }
        }
    }

    public void Update() {
        lifeSpan -= Time.deltaTime;
        if (lifeSpan < 0) {
            if (!gameObject.activeSelf)
                return;
            Destroyed?.Invoke(this, DestroyedReason.AGE);
            ProjectilePool.ReturnProjectile(this);
            return;
        }

        UpdateEvent?.Invoke(this, Time.deltaTime);
    }
}
