﻿using UnityEngine;
using System.Collections;

public class Melee : Spawnable {
    public SpawnType GetSpawnType => SpawnType.MELEE;
    WeaponSlot slot;
    public WeaponSlot GetSlot => slot;
}
