﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Summon : Spawnable {
    public SpawnType GetSpawnType => SpawnType.SUMMON;
    WeaponSlot slot;
    public WeaponSlot GetSlot => slot;
}
