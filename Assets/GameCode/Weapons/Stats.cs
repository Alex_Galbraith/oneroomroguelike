﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ProjectileStats {
    SPEED,
    SIZE,
    GRAVITY,
    DRAG,
    LIFESPAN,
    KNOCKBACK,
    MAX_HITS,
    BOUNCE_ENEMIES,
    BOUNCE_FRIENDLIES,
    AFFECT_ENEMIES,
    AFFECT_FRIENDLIES,
    PEIRCE_WALLS,
    TICK_MULTIPLE
}

public enum WeaponStats {
    MANA,
    COOLDOWN,
    HEALTH_COST,
    SELF_KNOCKBACK,
    ACCURACY,
    CRIT_CHANCE,
    CRIT_DAMAGE,
    SHOT_COUNT,
    PRIMARY_DAMAGE
}

public enum MobStats {
    MAX_HEALTH,
    HEALTH_REGEN,
    MAX_ARMOR,
    ARMOR_REGEN,
    MAX_MANA,
    MANA_REGEN,
    MOVE_SPEED,
    MOVE_ACC,
    JUMP_HEIGHT,
    JUMPS,
    GRAVITY,
    WALL_JUMP,
    DAMAGE_RECEIVED,
    LUCK,
    KNOCKBACK_TAKEN
}