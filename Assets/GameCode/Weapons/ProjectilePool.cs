﻿using UnityEngine;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;

public abstract class ProjectilePool {
    private static readonly int MAX_PROJECTILES = 256;
    private static Projectile[] projectiles = new Projectile[MAX_PROJECTILES];
    private static Queue<int> DeadProjectiles = new Queue<int>();
    private static HashSet<int> DeadProjectilesSet = new HashSet<int>();
    private static Sprite defaultSprite = Resources.LoadAll<Sprite>("Sprites/BasicBullet")[0];
    public static int LiveCount {
        get;
        private set;
    }
    private static int CreatedProjectiles = 0;

    public static Projectile GetProjectile() {
        //there are no spawnable projectiles
        if (DeadProjectiles.Count == 0) {
            //We can spawn
            if (CreatedProjectiles < MAX_PROJECTILES) {
                Projectile p = CreateProjectile();
                projectiles[p.ID] = p;
                LiveCount++;
                return p;
            }
            return null;
        }
        //get a dead projectile and revive it
        if (DeadProjectiles.Count!=0) {
            LiveCount++;
            int id = DeadProjectiles.Dequeue();
            DeadProjectilesSet.Remove(id);
            return projectiles[id];
        }
        return null;
    }

    public static void ReturnProjectile(Projectile p) {
        if (DeadProjectilesSet.Contains(p.ID)) { 
            Debug.LogWarning("duplicate ID returned");
            return;
        }
        p.Clean();
        LiveCount--;
        DeadProjectilesSet.Add(p.ID);
        DeadProjectiles.Enqueue(p.ID);
    }

    private static Projectile CreateProjectile() {
        GameObject go = new GameObject("Projectile");
        Projectile proj = go.AddComponent<Projectile>();
        proj.ID = CreatedProjectiles++;
        proj.Renderer = go.AddComponent<SpriteRenderer>();
        proj.Renderer.sprite = defaultSprite;
        proj.Rigidbody = go.AddComponent<Rigidbody2D>();
        proj.Rigidbody.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
        proj.Collider = go.AddComponent<CircleCollider2D>();
        proj.TriggerChild = new GameObject("TriggerChild");
        proj.Trigger = proj.TriggerChild.AddComponent<CircleCollider2D>();
        proj.TriggerChild.transform.SetParent(proj.transform, false);
        proj.Clean();
        
        return proj;
    }

}
