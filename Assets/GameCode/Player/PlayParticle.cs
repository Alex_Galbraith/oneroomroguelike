﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayParticle : StateMachineBehaviour
{
    [System.Serializable]
    public struct TransitionParticle {
        [SerializeField, Tooltip("Index of this particle in the ParticleAnimatorAdaptor")]
        public int ParticleID;
        [SerializeField, Tooltip("Leave empty for any")]
        public string TransitionName;
    }
    public TransitionParticle[] EnterParticles;
    internal ParticleAnimatorAdaptor adaptor;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        foreach (TransitionParticle i in EnterParticles) {
            if (string.IsNullOrEmpty(i.TransitionName) || adaptor.lastState.IsName(i.TransitionName))
            try {
                adaptor.particles[i.ParticleID].Play();
            }catch (System.NullReferenceException) {
                Debug.LogError("Particle " + i + " does not exist in adaptor.");
            }
        }
        adaptor.lastState = animatorStateInfo;
    }
}
