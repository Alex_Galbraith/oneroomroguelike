﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static PlatformerController.ControllerState;


[RequireComponent(typeof(BoxCollider2D), typeof(Rigidbody2D))]
public class PlatformerController : Squashable, IMobController
{
    public enum ControllerState {
        WALKING,
        SLIDING,
        JUMPING,
        FALLING,
        WALL_SLIDING
    }
    public Animator animator;
    public ControllerState state = WALKING;
    public MobData Data;

    [Header("Movement")]
    public float    AirControlAcc = 1f;
    public bool     WallRefreshesJump = false;
    public float    WallJumpBonus = 0.1f;
    public float    slideFriction = 0.1f;
    public float    jumpVelocityPreservation = 0.5f;

    [Header("Stepping and Slopes")]
    public bool     StepUp = true;
    public float    MaxStepUp = 1;
    public float    StepUpHorDelta = .1f;
    [Range(0, Mathf.PI), Tooltip("Max walkable slope in radians")]
    //55 degrees in radians default
    public float    SlopeLimit = 0.959931f;
    float           WallSlideLimit = 10f / 180f * Mathf.PI;
    //public AnimationCurve slopeSpeed;

    [Header("Physics")]
    public float    fastFallMult = 2f;
    public float    normalBias = 0.05f;
    public float    drag = 0.01f;
    public float    groundCheckDist = 0.1f;
    public float    fallCheckDist = 1f;
    public uint     moveIterations = 3;

    private float   endKnockbackThreshold = 0.2f;
    public float    knockbackDecay = 1f;

    [Header("Collisions")]
    public ContactFilter2D  terrainFilter;
    public ContactFilter2D  oneWayFilter;
    public bool             CollideOneWay = true;

    private Vector2 lastVel;
    public Vector2 velocity;
    public override Vector2 Velocity {
        get {
            return velocity;
        }
    }

    new private BoxCollider2D collider;
    new private Rigidbody2D rigidbody;

    public bool Jumping {
        get {
            return state == JUMPING;
        }
    }

    public bool Falling {
        get {
            return state == FALLING;
        }
    }

    public bool Crouching {
        get {
            return crouching;
        }
    }

    public void KnockBack(Vector2 velocity, float time, bool force = false) {
        KnockedTime = time;
        if (force)
            this.velocity = velocity;
        else
            this.velocity += velocity;
        KnockedBack = true;
    }
    public bool KnockedBack {
        get;
        private set;
    }
    public float KnockedTime {
        get;
        private set;
    }


    private Vector2 accelleration;
    public override Vector2 Accelleration {
        get {
            return accelleration;
        }

    }
    private bool _grounded;
    public override bool Grounded {
        get { return _grounded; }
    }

    public Vector2 GroundedPos {
        get;
        private set;
    }
    private bool facingRight;
    public override bool FacingRight {
        get { return facingRight; }
    }

    public float SlopeAngle {
        get;
        private set;
    }


    public override float SquashVert => 1;

    private float NormToAngle(Vector2 norm) {
        //Since we would be dotting against Vector2.Up, i.e., (0,1), we can just take the y component
        float dot = norm.normalized.y;
        return Mathf.Acos(dot);
    }

    // Start is called before the first frame update
    void Start()
    {
        collider = GetComponent<BoxCollider2D>();
        rigidbody = GetComponent<Rigidbody2D>();
    }

    bool stepUp(Vector2 remainingDelta) {
        Vector2 init = transform.position;
        CastAndMove(Vector2.up * MaxStepUp, Vector2.up * MaxStepUp, out _, out _, normalBias, false);
        CastAndMove(Vector2.right * StepUpHorDelta * Mathf.Sign(remainingDelta.x), Vector2.right * StepUpHorDelta * Mathf.Sign(remainingDelta.x), out _, out _, 0.05f, false);
        RaycastHit2D hit;
        bool h = CastAndMove(Vector2.down * MaxStepUp, Vector2.down * MaxStepUp, out _, out _, out hit, normalBias, false);
        if (h) {
            if (transform.position.y- init.y > 0.1f && NormToAngle(hit.normal) < SlopeLimit) {
                return true;
            }
        }
        transform.position = init;
        return false;
    }

    bool CastAndMove(Vector2 velocity, Vector2 remainingDelta, out Vector2 newVelocity, out Vector2 newRemaining, float normalBias = 0.05f, bool stepup = true) {
        return CastAndMove(velocity, remainingDelta, out newVelocity, out newRemaining, out _, normalBias, stepup);
    }


    bool getHit(bool COW, Vector3 remainingDelta, out RaycastHit2D hit) {
        RaycastHit2D[] hits = new RaycastHit2D[1];
        int hitCount = collider.Cast(remainingDelta, terrainFilter, hits, remainingDelta.magnitude);
        hit = hits[0];
        if (COW) {
            RaycastHit2D[] hitsOW = new RaycastHit2D[3];
            int hitCountOW = collider.Cast(remainingDelta, oneWayFilter, hitsOW, remainingDelta.magnitude);
            for (int i = 0; i<hitCountOW; i++) {
                if ((!hit || hitsOW[i].distance < hit.distance) && hitsOW[i].distance > 0) {
                    hit = hitsOW[i];
                    hitCount++;
                    //Hits are returned in order of distance anyway so we can break once we find one
                    break;
                }
            }
        }

        return hitCount > 0;
    }

    bool CastAndMove(Vector2 velocity, Vector2 remainingDelta, out Vector2 newVelocity, out Vector2 newRemaining, out RaycastHit2D hit, float normalBias = 0.05f, bool stepup = true) {
        //Set up one way platform collisions
        bool COW = CollideOneWay && !crouching;
        Vector3 opos = rigidbody.position = transform.position;
        bool didHit = getHit(COW, remainingDelta, out hit);
        //If we didnt hit, just move
        if (!didHit) {
            transform.Translate(remainingDelta);
            newRemaining = Vector3.zero;
            newVelocity = velocity;
            return false;
        }
        //We did hit
        else {
            //Move the character
            transform.position = hit.centroid + normalBias * hit.normal;
            //Remove moved vel from remaining
            newRemaining = remainingDelta + (Vector2)(opos - transform.position);
            newVelocity = velocity - (Vector2.Dot(velocity, hit.normal) * hit.normal);
            //Try step up
            if (Mathf.Abs(newVelocity.x) < 0.01f && Mathf.Abs(velocity.x)>0.01f && stepup) {
                if (stepUp(remainingDelta)) {
                    newVelocity = velocity;
                    return true;
                }
            }
            //Make remaining velocity change when hitting something
            newRemaining = newRemaining - Vector2.Dot(newRemaining, hit.normal) * hit.normal;
            return true;
        }
    }


    bool crouching = false;
    public float lateJumpLeniency = 0.2f;
    private float lateJumpTime;
    private int remainingAirJumps = 0;
    private bool lastGrounded = false;
    private bool jumpKey = false;
    private bool lastJump = false;
    private Vector2 lastJumpNormal;
    // Update is called once per frame
    private void Update() {
        jumpKey = Input.GetButtonDown("Jump") | jumpKey;
    }

    void FixedUpdate() {

        float gravity = Data.GetStat(MobStats.GRAVITY);
        float fastFallGravity = Data.GetStat(MobStats.GRAVITY)*fastFallMult;
        int AirJumps = (int)Data.GetStat(MobStats.JUMPS) - 1;
        float HorRunSpeed = Mathf.Max(0,Data.GetStat(MobStats.MOVE_SPEED));
        float RunAcc = Data.GetStat(MobStats.MOVE_ACC);
        float JumpForce = Data.GetStat(MobStats.JUMP_HEIGHT);
        bool WallJump = Data.GetStat(MobStats.WALL_JUMP) > 0;


        //apply gravity if we arent touching the ground
        //or we are crouching and not travelling upwards
        //The reason for not travelling upwards is so that sliding up slopes doesnt become lame.
        if (!lastGrounded || (crouching&&velocity.y<=0))
            velocity += Vector2.down * (crouching? fastFallGravity : gravity) * Time.fixedDeltaTime;

        KnockedTime -= Time.fixedDeltaTime;
        if (KnockedTime <= 0)
            KnockedBack = false;
        if (KnockedBack) {
            velocity *= knockbackDecay;
            if (Vector2.SqrMagnitude(velocity) < endKnockbackThreshold)
                KnockedBack = false;
        }
        Vector2 posn = transform.position;
        //Move
        Vector2 remainingDelta = velocity * Time.fixedDeltaTime;
        Vector2 vn = velocity;
        int iter = 0;
        while (remainingDelta.sqrMagnitude > 0 && iter++ < moveIterations)
            CastAndMove(velocity, remainingDelta, out velocity, out remainingDelta, normalBias, StepUp);
        RaycastHit2D hit;

        //Check grounded
        getHit(CollideOneWay && !crouching, Vector2.down * fallCheckDist, out hit);
        _grounded = false;
        bool canFloorJump = false;
        float mvRight = Input.GetAxis("Horizontal");
        float mvUp = Input.GetAxis("Vertical");
        bool jump = jumpKey && !lastJump;
        bool jumpHeld = Input.GetButton("Jump");
        Vector2 collisionNormal = (velocity - vn).normalized;
        float collisionWallAngle = Mathf.Min(Mathf.Acos(collisionNormal.x), Mathf.Acos(-collisionNormal.x)) ;

        if (Jumping && velocity.y <= 0.1f)
            state = FALLING;

        if ((!hit || hit.distance > groundCheckDist) && !Jumping) {
            state = FALLING;
        }

        if (hit){
            if (hit.distance < groundCheckDist && !Jumping) {
                
                if ((SlopeAngle = NormToAngle(hit.normal)) < SlopeLimit) {
                    lastJumpNormal = Vector2.up;
                    _grounded = true;
                    GroundedPos = hit.point;
                    lateJumpTime = Time.time;
                    remainingAirJumps = AirJumps;
                    if (Mathf.Abs(velocity.x) <=  HorRunSpeed && !crouching && !KnockedBack)
                        state = WALKING;
                    else
                        state = SLIDING;
                }
                else {
                    if (!Jumping && collisionNormal.sqrMagnitude > 0 && Mathf.Abs(collisionWallAngle) <= WallSlideLimit && !KnockedBack) {
                        state = WALL_SLIDING;
                    }
                    else { 
                        state = SLIDING;
                    }
                }
            }
            else {
                if (!Jumping && collisionNormal.sqrMagnitude > 0 && Mathf.Abs(collisionWallAngle) <= WallSlideLimit && !KnockedBack) {
                    state = WALL_SLIDING;
                }
            }
        }
        else {
            if (!Jumping && collisionNormal.sqrMagnitude > 0 && Mathf.Abs(collisionWallAngle) <= WallSlideLimit && !KnockedBack) {
                state = WALL_SLIDING;
            }
        }

        if (mvUp < -0.1f) {
            Crouch();
        }
        else if (mvUp >=0) {
            Crouch(false);
        }

        float standingAngle = NormToAngle(hit.normal);

        switch (state) {
            case WALKING:
                Vector2 tangent = new Vector2(hit.normal.y, -hit.normal.x);
                velocity += tangent * RunAcc * mvRight;
                if (velocity.sqrMagnitude > HorRunSpeed * HorRunSpeed) {
                    velocity = velocity.normalized * HorRunSpeed * 0.99f;
                }
                if (mvRight == 0)
                    velocity *= 0.6f;
                break;
            case SLIDING:
                goto case FALLING;
            case JUMPING:
                if (!jumpHeld)
                    velocity.y = velocity.y * 0.6f;
                goto case FALLING;
            case FALLING:
                velocity = velocity * (1 - drag * (crouching?0:1));
                float targetVal;
                //If we are moving faster in the direcion we want to go, maintain it to allows sliding
                if (velocity.x * mvRight > 0) {
                    targetVal = Mathf.Max(Mathf.Abs(velocity.x), Mathf.Abs(mvRight) * HorRunSpeed);
                }
                else {
                    targetVal = Mathf.Abs(mvRight) * HorRunSpeed;
                }
                float delta = targetVal * Mathf.Sign(mvRight) - velocity.x;
                velocity.x += Mathf.Min(Mathf.Abs(delta), AirControlAcc) * Mathf.Sign(delta);
                break;
            case WALL_SLIDING:
                velocity.y *= (1 - slideFriction);
                if (WallRefreshesJump) 
                    remainingAirJumps = AirJumps;
                if (WallJump) { 
                    lateJumpTime = Time.time + lateJumpLeniency*0.5f;
                    lastJumpNormal = collisionNormal;
                    lastJumpNormal.y += 1;
                    lastJumpNormal.y += mvUp;
                    lastJumpNormal.Normalize();
                    lastJumpNormal.y += mvUp*WallJumpBonus;
                }
                goto case FALLING;
        }

        float lateJumpTimer = Time.time - lateJumpTime;
        if (lateJumpTimer < lateJumpLeniency) {
            canFloorJump = true;
        }

        //Stop slope hopping
        if (lastGrounded && !Grounded && velocity.y > 0 && Mathf.Abs(velocity.x) < 0.1f && state == WALKING)
            velocity.y = 0;
        if(lastGrounded && !Grounded) {
            CastAndMove(Vector2.down, Vector2.down * groundCheckDist, out _, out _, 0, false);
            //manual bias
            transform.position += Vector3.up * normalBias;
        }

        //Jump
        if (jump && (canFloorJump || remainingAirJumps > 0)) {
            if (!canFloorJump) {
                remainingAirJumps--;
                lastJumpNormal = Vector2.up;
            }
            //ToDo prevent slow downs
            velocity.y *= jumpVelocityPreservation;
            velocity.y += lastJumpNormal.y * JumpForce;
            velocity.x += lastJumpNormal.x * JumpForce;
            state = JUMPING;
            animator.SetTrigger("Jump");
        }

        lastGrounded = Grounded;
        lastJump = jump;
        jumpKey = false;
        accelleration = velocity - lastVel;
        lastVel = velocity;
        if (velocity.x > 0)
            facingRight = true;
        else if (velocity.x < 0)
            facingRight = false;

        //Animator
        animator.SetFloat("Speed", Mathf.Abs(velocity.x) / HorRunSpeed);
        animator.SetBool("Grounded", Grounded);
        animator.SetBool("Crouching", crouching);
    }

    internal void TryUnCrouch() {
        if (!crouching)
            return;
        RaycastHit2D[] hits = new RaycastHit2D[1];
        if (collider.Cast(Vector2.down, terrainFilter, hits, collider.size.y) > 0) {
            transform.position = hits[0].centroid + normalBias * hits[0].normal;
        }
        Crouch(false);
    }

    public void Crouch(bool crouch = true) {
        if (crouch) {
            if (!crouching) { 
                collider.size = new Vector2(collider.size.x, collider.size.y / 2);
                //collider.offset = collider.offset + Vector2.up * collider.size.y/2;
                CastAndMove(Vector2.down, Vector2.down * collider.size.y, out _, out _, 0, false);
                //manual bias
                transform.position += Vector3.up * normalBias;
                crouching = true;
            }
        }
        else {
            if (crouching) {
                RaycastHit2D[] hits = new RaycastHit2D[1];
                if (collider.Cast(Vector2.down, terrainFilter, hits, collider.size.y) == 0) {
                    //transform.Translate(new Vector3(0, -collider.size.y, 0));
                    //collider.offset = collider.offset + Vector2.down * collider.size.y / 2;
                    collider.size = new Vector2(collider.size.x, collider.size.y * 2);

                    crouching = false;
                }else
                if (collider.Cast(Vector2.up, terrainFilter, hits, collider.size.y) == 0) {
                    transform.Translate(new Vector3(0, collider.size.y, 0));
                    //collider.offset = collider.offset + Vector2.down * collider.size.y / 2;
                    collider.size = new Vector2(collider.size.x, collider.size.y * 2);
                    
                    crouching = false;
                }
            }
        }
    }
}
