﻿using UnityEngine;
using System.Collections;
using System;


public class Player : Mob {
    public WeaponSlot WeaponSlotA;
    public WeaponSlot WeaponSlotB;
    public WeaponSlot WeaponSlotC;

    private void Awake() {
        WeaponSlotA = ScriptableObject.CreateInstance<WeaponSlot>();
        WeaponSlotB = ScriptableObject.CreateInstance<WeaponSlot>();
        WeaponSlotC = ScriptableObject.CreateInstance<WeaponSlot>();
        WeaponSlotA.player = WeaponSlotB.player = WeaponSlotC.player = this;
        WeaponSlotA.owner = WeaponSlotB.owner = WeaponSlotC.owner = this;
        Weapon w = new BasicGun();
        WeaponSlotA.Weapon = w;
        data = GetComponent<MobData>();
    }

    private void Update() {
        if (Input.GetAxis("Fire1") > 0) {
            WeaponSlotA.Held();
        }
        if (Input.GetAxis("Fire2") > 0) {
            WeaponSlotB.Held();
        }
        if (Input.GetAxis("Fire3") > 0) {
            WeaponSlotC.Held();
        }

        if (Input.GetButtonDown("Fire1"))
            WeaponSlotA.Pressed();
        if (Input.GetButtonDown("Fire2"))
            WeaponSlotB.Pressed();
        if (Input.GetButtonDown("Fire3"))
            WeaponSlotC.Pressed();

        if (Input.GetButtonUp("Fire1"))
            WeaponSlotA.Released();
        if (Input.GetButtonUp("Fire2"))
            WeaponSlotB.Released();
        if (Input.GetButtonUp("Fire3"))
            WeaponSlotC.Released();
    }

}
