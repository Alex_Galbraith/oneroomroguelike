﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
[RequireComponent (typeof(Camera))]
public class ShockwaveEffect : MonoBehaviour {
    public Material mat;
    private Camera cam;
    private List<ShockData> sdat = new List<ShockData>();
    private static ShockwaveEffect singleton;
    private void Awake() {
        singleton = this;
        cam = GetComponent<Camera>();
    }
    private void OnRenderImage(RenderTexture source, RenderTexture destination) {
        Matrix4x4 world2Screen = Camera.main.projectionMatrix * Camera.main.worldToCameraMatrix;
        Matrix4x4 inv = world2Screen.inverse;
        mat.SetMatrix("_CamToWorld", inv);
        mat.SetFloat("_Scale", cam.orthographicSize);

        Vector4[] positions = new Vector4[10];
        Vector4[] data       = new Vector4[10];


        int i = 0;

        for (; i < sdat.Count; i++) {
            ShockData sd = sdat[sdat.Count - 1 - i];
            if (i >= 10)
                break;
            if (sd.strength == 0) {
                sdat.RemoveAt(sdat.Count - 1 - i);
                i--;
                continue;
            }
            data[i] = new Vector4(1, sd.radius, sd.strength, sd.tightness);
            positions[i] = sd.position;
        }
        //Makes sure rest are disabled
        mat.SetInt("_Num", i);
        for (; i < 10; i++)
            data[i] = Vector4.zero;



        mat.SetVectorArray("_Data", data);
        mat.SetVectorArray("_Points", positions);

        Graphics.Blit(source, destination, mat);
    }
    public static void addShock(Vector3 pos, float radFrom, float radTo, float str, float tight, float lifespan) {
        if(singleton.sdat.Count<10)
            singleton.sdat.Add(new ShockData(pos, radFrom, radTo, str, tight, lifespan));
    }
    class ShockData {
        public Vector3 position;
        public float radius;
        public float strength;
        public float tightness;
        public ShockData(Vector3 pos, float radFrom, float radTo, float str, float tight, float lifespan) {
            this.position = pos;
            this.strength = str;
            this.radius = radFrom;
            this.tightness = tight;
            DOTween.To(() => radius, x => radius = x, radTo, lifespan);
            DOTween.To(() => strength, x => strength = x, 0, lifespan);
        }
    }
}
