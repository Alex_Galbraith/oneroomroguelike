﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
[RequireComponent(typeof(MeshRenderer))]
public class RingBurst : MonoBehaviour {
    private Vector3 centerPos;
    private Material material;
    public Color colour = Color.white;
    public float lerpTo = 10;
    public float radius = 0;
    public float time = 0.5f;
    public float burst = 0;
    public float thickness = 0.01f;
    public float thicknessTo = 0.01f;
    public float delay = 0.1f;
	// Use this for initialization
	void Start () {
        centerPos = transform.position;
        Camera cam = Camera.main;

        float pos = Vector3.Dot(centerPos - cam.transform.position, cam.transform.forward.normalized);

        transform.position = cam.transform.position + cam.transform.forward * pos;
        centerPos.z = transform.position.z;

        float h = Mathf.Tan(cam.fieldOfView * Mathf.Deg2Rad * 0.5f) * pos * 2f;

        transform.localScale = new Vector3(h * cam.aspect, 0, h);
        material = new Material(GetComponent<MeshRenderer>().material);
        GetComponent<MeshRenderer>().material = material;
    }

    public void Activate() {
        if (delay <= 0) {
            play();
        }
        else {
            material.SetFloat("_Thickness", 0);
            material.SetFloat("_Radius", 0);
            this.Invoke("play", delay);
        }
    }
	
    private void play() {
        material.SetVector("_Center", centerPos);
        material.SetColor("_Colour", colour);
        material.SetFloat("_BurstRate", burst);
        material.SetFloat("_Thickness", thickness);
        material.SetFloat("_Radius", radius);
        material.DOFloat(lerpTo, "_Radius", time);
        material.DOFloat(thicknessTo, "_Thickness", time).OnComplete(destroy);
    }

    private void destroy() {
        Destroy(this.gameObject);
    }
}
