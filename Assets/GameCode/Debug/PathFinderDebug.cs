﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static PathFinder;

public class PathFinderDebug : MonoBehaviour
{
    public PathFindingScheduler pathFinder;
    public PathFindingGrid pgrid;
    private bool pathSet = false;
    private bool pathReq = false;
    private int pathCount;
    public bool doLinks = true;
    public int maxHeight = 2;
    public float gravity = 80;
    public float hspeed = 8;
    public PathFindingGrid.AStarType mask = PathFindingGrid.AStarType.WALKABLE;
    Instruction[] i;
    // Start is called before the first frame update
    void Start()
    {
        i = new Instruction[32];
    }

    private void PathFindingComplete(PathFindingResult result, int count) {
        if (result == PathFindingResult.SUCCESS || result == PathFindingResult.NO_PATH) {
            pathSet = true;
            pathCount = count;
        }
        pathReq = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (pathSet) {
            if (pathCount >0)
                Debug.DrawLine(transform.position, pgrid.DataCellToWorldPos(i[pathCount-1].posx, i[pathCount-1].posy), Color.red);
            for (int j = 0; j < pathCount - 1; j++) {
                Debug.DrawLine(pgrid.DataCellToWorldPos(i[j].posx, i[j].posy) , pgrid.DataCellToWorldPos(i[j + 1].posx, i[j + 1].posy), Color.red);
            }
            pathReq = false;
        }
        if (!pathReq) {
            pathFinder.PathFind( transform.position, Camera.main.ScreenToWorldPoint(Input.mousePosition), mask, PathFindingComplete, i, doLinks, maxHeight, hspeed, gravity);
            pathReq = true;
            pathSet = false;
        }
    }
}
