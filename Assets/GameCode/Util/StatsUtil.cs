﻿using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Mathematics; 
public abstract class StatsUtil {
    /// <summary>
    /// <para>Converts (-inf,inf) to (0,inf) with the property f(x) * f(-x) = 1</para>
    /// <para>E.G., f(-8) = 1/9;  f(8) = 9; f(0) = 1 </para>
    /// <para>f(x) = abs(x) ^ signum(x)</para>
    /// </summary>
    /// <param name="mult"></param>
    /// <returns></returns>
    [BurstCompile]
    public static float MultSpaceToLinearSpace(float mult) {
        return mult < 0 ? 1 / (1 - mult) : mult + 1;
        //return math.pow(math.abs(mult)+1, math.sign(mult));
    }
   
}
