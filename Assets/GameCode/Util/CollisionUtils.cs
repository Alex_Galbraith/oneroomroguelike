﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Unity.Mathematics.math;
using Unity.Mathematics;
using Unity.Burst;
public abstract class CollisionUtils
{

    /// <summary>
    ///Gets the important mob classes for collisions. Checks in children.
    /// </summary>
    /// <param name="collider">Collider that we want the data from</param>
    /// <param name="damageable">Damagable interface for the collider's game object.</param>
    /// <param name="manipulator">Mob data manipulator for this mob. </param>
    public static void GetMobClasses(Collider2D collider, out IDamagable damageable, out Mob manipulator) {
        if (collider == null) {
            damageable = manipulator = null;
            return;
        }
        damageable = collider.GetComponentInChildren<IDamagable>();
        if (damageable == null) {
            manipulator = null;
            return;
        }

        if (damageable is Mob) {
            manipulator = (Mob)damageable;
            return;
        }

        manipulator = collider.GetComponentInChildren<Mob>();
    }
    
    //FROM: https://stackoverflow.com/questions/1073336/circle-line-segment-collision-detection-algorithm
    
    [BurstCompile]
    public static bool RayCircleIntersection(float rsx, float rsy, float rdirx, float rdiry, float ccx, float ccy, float radius) {

        float fx = ccx - rsx;
        float fy = ccy - rsy;
        float a = rdirx * rdirx + rdiry * rdiry;
        float b = 2 * (fx * rdirx + fy * rdiry);
        float c = fx*fx + fy*fy - radius * radius;

        float discriminant = b * b - 4 * a * c;
        if (discriminant < 0) {
            // no intersection
            return false;
        }
        else {

            //cull based on magnitude

            // ray didn't totally miss sphere,
            // so there is a solution to
            // the equation.

            discriminant = sqrt(discriminant);

            // either solution may be on or off the ray so need to test both
            // t1 is always the smaller value, because BOTH discriminant and
            // a are nonnegative.
            float t1 = (-b - discriminant) / (2 * a);
            float t2 = (-b + discriminant) / (2 * a);

            // 3x HIT cases:
            //          -o->             --|-->  |            |  --|->
            // Impale(t1 hit,t2 hit), Poke(t1 hit,t2>1), ExitWound(t1<0, t2 hit), 

            // 3x MISS cases:
            //       ->  o                     o ->              | -> |
            // FallShort (t1>1,t2>1), Past (t1<0,t2<0), CompletelyInside(t1<0, t2>1)

            if (t1 >= 0 && t1 <= 1) {
                // t1 is the intersection, and it's closer than t2
                // (since t1 uses -b - discriminant)
                // Impale, Poke
                return true;
            }

            // here t1 didn't intersect so we are either started
            // inside the sphere or completely past it
            if (t2 >= 0 && t2 <= 1) {
                // ExitWound
                return true;
            }

            // no intn: FallShort, Past, CompletelyInside
            return false;
        }
    }
}
