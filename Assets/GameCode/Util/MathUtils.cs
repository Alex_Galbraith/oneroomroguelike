﻿using UnityEngine;
using System.Collections;

public abstract class MathUtils {
    [Unity.Burst.BurstCompile]
    public static Vector2 Rotate(Vector2 vector, float rads) {
        float s = Mathf.Sin(rads);
        float c = Mathf.Cos(rads);
        return new Vector2(
            vector.x * c - vector.y * s,
            vector.y * c + vector.x * s
     );
    }
}
