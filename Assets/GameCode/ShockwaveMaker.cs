﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShockwaveMaker : MonoBehaviour {
    public float radiusFrom = 1;
    public float radiusTo = 20;
    public float strength = 0.01f;
    public float tightness = 30;
    public float duration = 0.6f;
    public float delay = 0f;
    // Use this for initialization
    void Start () {
        if (delay == 0)
            shock();
        else
            Invoke("shock", delay);
    }
    private void shock() {
        ShockwaveEffect.addShock(transform.position, radiusFrom, radiusTo, strength, tightness, duration);
    }

}
