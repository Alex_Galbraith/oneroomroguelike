﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformFlipper : MonoBehaviour
{
    public Squashable squashable;
    // Update is called once per frame
    private Vector3 v = Vector3.one;
    void Update() { 
        if (squashable.Velocity.x != 0)
            v.x = Mathf.Sign(squashable.Velocity.x);
        //transform.localRotation = Quaternion.Euler(0, 0, v.x < 0 ? Mathf.PI : 0);
        transform.localScale = v;
    }
}
