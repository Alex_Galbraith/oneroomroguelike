﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPBarVisuals : MonoBehaviour
{
    public float health;
    public float maxHealth;
    public float shield;

    public float healthPerSegment;
    public float shieldPerSegment;

    public Transform    topper;
    public Transform    ShieldBot;
    public Transform[]  healthSegments;
    public Transform[]  shieldSegments;
    public Transform    healthBar;
    public float        healthBarMaxYScale;
    public Transform    shieldBar;
    public float        shieldBarMaxYScale;

    public Mob mob;

    public void UpdateBar() {
        if (mob == null)
            return;
        health = mob.data.Health;
        shield = mob.data.Armor;
        maxHealth = mob.data.GetStat(MobStats.MAX_HEALTH);

        float maxHealthCap = healthPerSegment * (healthSegments.Length);
        float maxShieldCap = shieldPerSegment * shieldSegments.Length;
        //Number of health segments
        int numHS = Mathf.CeilToInt(maxHealth/healthPerSegment);
        //Number of shield segments
        int numSS = Mathf.CeilToInt(shield / shieldPerSegment);

        //enable/disable health segments
        for(int i = 0; i < healthSegments.Length; i++) {
            if (i < numHS-1)
                healthSegments[i].gameObject.SetActive(true);
            else {
                healthSegments[i].gameObject.SetActive(false);
            }
        }
        //place topper on top
        topper.position = healthSegments[numHS - 1].transform.position;
        //enable/disable shield segments
        for (int i = 0; i < shieldSegments.Length; i++) {
            if (i < numSS)
                shieldSegments[i].gameObject.SetActive(true);
            else {
                shieldSegments[i].gameObject.SetActive(false);
            }
        }

        ShieldBot.gameObject.SetActive(numSS > 0);

        Vector2 v = healthBar.localScale;
        v.y = health / maxHealthCap * healthBarMaxYScale;
        healthBar.localScale = v;

        v = shieldBar.localScale;
        v.y = shield / maxShieldCap * shieldBarMaxYScale;
        shieldBar.localScale = v;
    }

    public void Start() {
        mob.OnPostDamage += OnMobDamage;
        UpdateBar();
    }

    public void OnMobDamage(DamageEvent d) {
        
        UpdateBar();
    }

    public void OnValidate() {
        UpdateBar();
    }
}
