﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleAnimatorAdaptor : MonoBehaviour
{
    public Animator animator;
    public ParticleSystem[] particles;
    internal AnimatorStateInfo lastState;
    // Start is called before the first frame update
    void Start()
    {
        foreach (var v in animator.GetBehaviours<PlayParticle>()) {
            v.adaptor = this;
        }
    }
}
