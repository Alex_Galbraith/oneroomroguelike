﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Post/Shockwave"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Point("Point", Vector) = (1,1,1)
		_Radius("Radius", Float) = 1
		_Tightness("Tightness", Float) = 2
		_Strength("Strength", Float) = 1
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float3 worldPos : TEXCOORD1;
			};

			float4x4 _CamToWorld;
			float _Scale;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				float4 vo = mul(_CamToWorld, v.vertex * 2 - float4(1,1,1,1)); 
				o.worldPos = vo; 
				return o;
			}
			
			sampler2D _MainTex;
			float _Radius;
			float _Tightness, _Strength;
			float2 _Point;

			

			float3 _Points[10];
			float _Strengths[10];
			float _Radii[10];
			float _Tightnesses[10];

			fixed4 frag (v2f i) : SV_Target
			{
				
				int on = round(_Radius*_Radius - distance(i.uv, _Point));
				on = clamp(on, 0, 1);
				int off = 1 - on;
				float dist = (distance(i.worldPos, _Point));
				float sdist = clamp(dist / _Radius, 0, 1);
				float2 p = i.uv + pow((0.5-cos(sdist * 3.14159f *2)/2),_Tightness)*dist*normalize(i.worldPos - _Point)*_Strength;

				fixed4 col = tex2D(_MainTex, p);
				// just invert the colors
				return col;
			}
			ENDCG
		}
	}
}
