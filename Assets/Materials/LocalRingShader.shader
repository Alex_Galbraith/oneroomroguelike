﻿Shader "Unlit/LocalRingShader"
{
	Properties
	{
		_Radius("Current Radius", Float) = 0
		_Thickness("Thickness", Float) = 0
		_Colour("Colour", Color) = (1,1,1,1)
		_BurstRate("Burst Rate", Float) = 0
		_Center("Center Position", Vector) = (0,0,0)
	}
	SubShader
	{
		Tags{ "Queue" = "Transparent" "Render" = "Transparent" "IgnoreProjector" = "True" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float3 worldPos : TEXCOORD0;
			};

			float4 _Center;
			float _Radius;
			float _Thickness;
			float _BurstRate;
			float4 _Colour;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.worldPos = v.vertex;// mul(unity_ObjectToWorld, v.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				float distSq = dot(i.worldPos, _Center);
				float4 col = _Colour;
				clip(round(_Radius - distance(i.worldPos, _Center) + _Thickness));
				clip(round(distance(i.worldPos, _Center) - _Radius - _BurstRate*(_Thickness*_Radius)));
				return col;
			}
			ENDCG
		}
	}
}
