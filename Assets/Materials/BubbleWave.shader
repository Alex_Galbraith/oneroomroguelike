﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Post/Bubblewave"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float3 worldPos : TEXCOORD1;
			};

			float4x4 _CamToWorld;
			float _Scale;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				float4 vo = mul(_CamToWorld, v.vertex * 2 - float4(1,1,1,1)); 
				o.worldPos = vo; 
				return o;
			}

			sampler2D _MainTex;

			float2 _Points[10];
			//enabled, radius, strength, tightness
			float4 _Data[10];
			int _Num;

			fixed4 frag (v2f inp) : SV_Target
			{
				float2 p = inp.uv;

				for (int i = 0; i < _Num; i++) {
					float dist = distance( inp.worldPos, _Points[i] );
					float sdist = clamp( dist / _Data[i].y, 0, 1 );
					p += pow((sin(sdist * 3.14159f)), _Data[i].w)*dist*normalize(inp.worldPos - _Points[i])*_Data[i].z*(dist / _Data[i].y);
				}

				fixed4 col = tex2D(_MainTex, p);

				return col;
			}
			ENDCG
		}
	}
}
