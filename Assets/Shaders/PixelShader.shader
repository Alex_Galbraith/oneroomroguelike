﻿Shader "Sprites/PixelShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		//Pixels per unit
        _Res ("Resolution", Float) = 1024
		//_WorldPos is used to offset the pixel boundary, should be set in a C# script if 
		//you want to use it.
		_WorldPos ("World position", Vector) = (0,0,0,0)
		
    }
    SubShader
    {
        Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType" = "Plane"
        }
        LOD 100
		ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
		Cull Off
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma target 3.0
            #include "UnityCG.cginc"

			fixed _Res;
			fixed _PixelSize;
			//_WorldPos is used to offset this boundary, should be set in a C# script if 
			//you want to use it.
			fixed3 _WorldPos;

			//Round values down to nearest "pixel" boundary
			//_WorldPos is used to offset this boundary, should be set in a C# script if 
			//you want to use it.
			float3 quant(float3 q, float3 res){
                return floor((q - (_WorldPos % (1 / _Res))) *res)/res;
            }

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float4 color : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float4 opos : TEXCOORD1;
				float4 wpos : TEXCOORD2;
				float4 color : COLOR;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
				o.opos = v.vertex;
				o.wpos = mul(unity_ObjectToWorld, v.vertex);
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.color = v.color;
				_WorldPos = _WorldPos ;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				//screenspace derivative of uv
				//d_uvx/d_x, d_uvy/d_y
				float2  ddi  = float2(ddx(i.uv.x),ddy(i.uv.y));
				//screenspace derivative of uv
				//d_uvx/d_y, d_uvy/d_x
				float2  ddif  = float2(ddy(i.uv.x),ddx(i.uv.y));
				//Screenspace derivative of worldpos
				//d_wposx/d_x, d_wposy/d_y
				float2  ddw  = float2(ddx(i.wpos.x),ddy(i.wpos.y));
				//uv per worldpos
				//d_uvx/d_wposx, d_uvy/d_wposy
				float2 ddiw = ddi / ddw;
				//d_uvx/d_wposy, d_uvy/d_wposx
				float2 ddiwf = ddif / ddw;

				float2 uv = i.uv;
				//"pixelate" our world position
				float3 quantedW = quant(i.wpos.xyz, _Res);
				//Modify UVs by difference in wpos.
				//uv += d_wpos * d_uv/d_wpos
				uv = (uv + ((quantedW.xy - i.wpos.xy) * ddiw));
				uv += ((quantedW.yx - i.wpos.yx) * ddiwf);
                // sample the texture
                fixed4 col = tex2D(_MainTex, uv) * i.color;
                return col;
            }
            ENDCG
        }
    }
}
